package tanchuev.whattodo.ui.presenter;

import java.util.List;

import javax.inject.Inject;

import tanchuev.whattodo.data.provider.Provider;
import tanchuev.whattodo.data.service.EmploymentService;
import tanchuev.whattodo.di.scope.PerActivity;
import tanchuev.whattodo.model.Category;
import tanchuev.whattodo.model.Employment;
import tanchuev.whattodo.model.Location;
import tanchuev.whattodo.ui.dialog.CategoryListDialogFragment;
import tanchuev.whattodo.ui.dialog.LocationListDialogFragment;
import tanchuev.whattodo.ui.presenter.base.Presenter;
import tanchuev.whattodo.ui.view.SuggestEmploymentView;

/**
 * @author tanchuev
 */

@PerActivity
public class SuggestEmploymentPresenter extends Presenter<SuggestEmploymentView> {

	@Inject
	EmploymentService mEmploymentService;
	@Inject
	Provider<Class> mClassProvider;
	@Inject
	Provider<List<Category>> mCategoriesProvider;
	@Inject
	Provider<List<Location>> mLocationsProvider;

	@Inject
	SuggestEmploymentPresenter() {
	}

	public void createEmployment(Employment employment) {
		//TODO!!! change to mEmploymentService.suggest()
		execute(mEmploymentService.create(employment), this::doOnSuggested);
	}

	private void doOnSuggested(Boolean isSuggested) {
		mView.onSuccessfulSuggest();
	}

	public void pickCategory(List<Category> categories) {
		mCategoriesProvider.set(categories);
		mClassProvider.set(Category.class);
		mNavigator.showDialog(new CategoryListDialogFragment());
	}

	public void pickLocation(List<Location> locations) {
		mLocationsProvider.set(locations);
		mClassProvider.set(Location.class);
		mNavigator.showDialog(new LocationListDialogFragment());
	}

}