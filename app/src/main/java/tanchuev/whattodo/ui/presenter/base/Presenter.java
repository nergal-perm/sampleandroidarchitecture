package tanchuev.whattodo.ui.presenter.base;

import android.widget.Toast;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import tanchuev.whattodo.R;
import tanchuev.whattodo.rx.RxTaskExecutor;
import tanchuev.whattodo.navigation.NavigatorImpl;
import tanchuev.whattodo.ui.view.base.HasActionsView;
import tanchuev.whattodo.utils.ViewUtils;

/**
 * @author tanchuev
 */
public abstract class Presenter<TView extends HasActionsView> {
	// todo у Presenter несколько View или одна?
	// todo нужно ли восстанавливать подписки или
	// нужен ли механизм, который будет контролировать восстанавливать ли подписки или нет?

	@Inject
	protected NavigatorImpl mNavigator;
	@Inject
	RxTaskExecutor mTaskExecutor;

	protected TView mView;
	private boolean isViewAttached;
	private Subscription mCurrentSubscription;

	public Presenter() {
	}

	public void setView(TView view) {
		mView = view;
	}

	public void attachView() {
		isViewAttached = true;
	}

	public void detachView() {
		isViewAttached = false;
		mTaskExecutor.clearSubscriptions();
	}

	public void destroy() {
		mTaskExecutor.clearSubscriptions();
	}

	protected final <T> void executeIfNotBusy(Observable<T> observable,
											  final Action1<? super T> doOnNext/*response :)*/) {
		executeIfNotBusy(observable, doOnNext, null);
	}

	protected final <T> void executeIfNotBusy(Observable<T> observable,
									 final Action1<? super T> doOnNext,
									 final Action0 doOnCompleted) {
		executeIfNotBusy(observable, doOnNext, null, doOnCompleted);
	}

	protected final <T> void executeIfNotBusy(Observable<T> observable,
									 final Action1<? super T> doOnNext,
									 final Action1<Throwable> doOnError,
									 final Action0 doOnCompleted) {
		if (isViewAttached) {
			if (isTaskFinished()) {
				mCurrentSubscription = mTaskExecutor.executeAsync(observable, doOnNext, doOnError, doOnCompleted, mView);
			} else {
				showTaskNotFinishedError();
			}
		}
	}

	protected final <T> void execute(Observable<T> observable,
									 final Action1<? super T> doOnNext/*response :)*/) {
		execute(observable, doOnNext, null);
	}

	protected final <T> void execute(Observable<T> observable,
									 final Action1<? super T> doOnNext,
									 final Action0 doOnCompleted) {
		execute(observable, doOnNext, null, doOnCompleted);
	}

	protected final <T> void execute(Observable<T> observable,
									 final Action1<? super T> doOnNext,
									 final Action1<Throwable> doOnError,
									 final Action0 doOnCompleted) {
		if (isViewAttached) {
			mCurrentSubscription = mTaskExecutor.executeAsync(observable, doOnNext, doOnError, doOnCompleted, mView);
		}
	}

	protected boolean isTaskFinished() {
		return RxTaskExecutor.isNullOrUnsubscribed(mCurrentSubscription);
	}

	protected void showTaskNotFinishedError() {
		ViewUtils.showToastMessage(mView.getBaseActivity(), R.string.errorTaskNotFinished, Toast.LENGTH_SHORT);
	}
}
