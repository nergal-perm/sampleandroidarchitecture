package tanchuev.whattodo.ui.presenter;

import javax.inject.Inject;

import tanchuev.whattodo.di.scope.PerActivity;
import tanchuev.whattodo.ui.presenter.base.Presenter;
import tanchuev.whattodo.ui.view.AuthView;

/**
 * @author tanchuev
 */
@PerActivity
public class AuthPresenter extends Presenter<AuthView> {

	@Inject
	AuthPresenter() {
	}

	public void auth(String login, String password) {

	}

}