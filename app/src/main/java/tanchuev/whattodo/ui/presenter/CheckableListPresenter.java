package tanchuev.whattodo.ui.presenter;

import java.util.List;

import javax.inject.Inject;

import tanchuev.whattodo.data.provider.Provider;
import tanchuev.whattodo.data.service.CategoryService;
import tanchuev.whattodo.data.service.LocationService;
import tanchuev.whattodo.model.Category;
import tanchuev.whattodo.model.Location;
import tanchuev.whattodo.model.base.ModelName;
import tanchuev.whattodo.ui.presenter.base.Presenter;
import tanchuev.whattodo.ui.view.CheckableListView;

/**
 * @author tanchuev
 */

public class CheckableListPresenter<T extends ModelName> extends Presenter<CheckableListView> {

	@Inject
	CategoryService mCategoryService;
	@Inject
	LocationService mLocationService;
	@Inject
	Provider<Class> mClassProvider;
	@Inject
	Provider<List<T>> mCheckableObjectsProvider;

	@Inject
	CheckableListPresenter() {
	}

	public void getItems() {
		mView.getAdapter().removeAll();
		mView.getAdapter().setCheckedItems(mCheckableObjectsProvider.get());
		if (mClassProvider.get() == Category.class) {
			execute(mCategoryService.getAll(), this::onGetCategories);
		} else if (mClassProvider.get() == Location.class) {
			execute(mLocationService.getAll(), this::onGetLocations);
		}
	}

	private void onGetCategories(Category category) {
		//todo бесит unchecked call, но пока что не могу придумать, что с этим делать
		mView.getAdapter().add(category);
	}

	private void onGetLocations(Location location) {
		//todo бесит unchecked call, но пока что не могу придумать, что с этим делать
		mView.getAdapter().add(location);
	}

}