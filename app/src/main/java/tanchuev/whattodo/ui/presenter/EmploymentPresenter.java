package tanchuev.whattodo.ui.presenter;

import android.support.v4.util.Pair;

import java.util.List;

import javax.inject.Inject;

import tanchuev.whattodo.R;
import tanchuev.whattodo.data.provider.Provider;
import tanchuev.whattodo.data.service.EmploymentService;
import tanchuev.whattodo.data.service.UserService;
import tanchuev.whattodo.di.scope.PerActivity;
import tanchuev.whattodo.model.Category;
import tanchuev.whattodo.model.Employment;
import tanchuev.whattodo.model.Filter;
import tanchuev.whattodo.model.Location;
import tanchuev.whattodo.ui.dialog.CategoryListDialogFragment;
import tanchuev.whattodo.ui.dialog.LocationListDialogFragment;
import tanchuev.whattodo.ui.presenter.base.Presenter;
import tanchuev.whattodo.ui.view.EmploymentView;

/**
 * @author tanchuev
 */

@PerActivity
public class EmploymentPresenter extends Presenter<EmploymentView> {

	@Inject
	EmploymentService mEmploymentService;
	@Inject
	UserService mUserService;
	@Inject
	Provider<Class> mClassProvider;
	@Inject
	Provider<List<Category>> mCategoriesProvider;
	@Inject
	Provider<List<Location>> mLocationsProvider;

	private Employment mCurrentEmployment;

	@Inject
	EmploymentPresenter() {
	}

	public void like() {
		mView.setCurrentLoadingView(R.id.likeLoadingView);
		executeIfNotBusy(mEmploymentService.like(mCurrentEmployment.getId()), this::onLiked);
	}

	private void onLiked(Pair<Boolean, Long> response) {
		mView.setLikeState(response.first, response.second);
	}

	public void getEmployment() {
		mView.setCurrentLoadingView(R.id.mainLoadingView);
		executeIfNotBusy(mEmploymentService.getRandom(), this::onGetEmployment);
	}

	public void getEmployment(Filter filter) {
		mView.setCurrentLoadingView(R.id.mainLoadingView);
		executeIfNotBusy(mEmploymentService.getFiltered(filter), this::onGetEmployment);
	}

	private void onGetEmployment(Employment employment) {
		mView.clearView();
		mCurrentEmployment = employment;
		mView.setEmployment(employment);
	}

	public void pickCategory(List<Category> categories) {
		mCategoriesProvider.set(categories);
		mClassProvider.set(Category.class);
		mNavigator.showDialog(new CategoryListDialogFragment());
	}

	public void pickLocation(List<Location> locations) {
		mLocationsProvider.set(locations);
		mClassProvider.set(Location.class);
		mNavigator.showDialog(new LocationListDialogFragment());
	}
}