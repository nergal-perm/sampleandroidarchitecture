package tanchuev.whattodo.ui.presenter;

import javax.inject.Inject;

import tanchuev.whattodo.data.service.UserService;
import tanchuev.whattodo.di.scope.PerActivity;
import tanchuev.whattodo.model.User;
import tanchuev.whattodo.ui.presenter.base.Presenter;
import tanchuev.whattodo.ui.view.RegistrationView;

/**
 * @author tanchuev
 */

@PerActivity
public class RegistrationPresenter extends Presenter<RegistrationView> {

	@Inject
	UserService mUserService;

	@Inject
	RegistrationPresenter() {
	}

	public void registration(User user) {
		//todo user registration
	}

}