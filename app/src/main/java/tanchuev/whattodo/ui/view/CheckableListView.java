package tanchuev.whattodo.ui.view;

import tanchuev.whattodo.ui.adapter.CheckableListAdapter;
import tanchuev.whattodo.ui.view.base.AdapterView;

/**
 * @author tanchuev
 */

public interface CheckableListView extends AdapterView<CheckableListAdapter> {
}
