package tanchuev.whattodo.ui.view.base;

/**
 * @author tanchuev
 */
public interface LoadingView extends ActionView {

	void showLoading();

	void hideLoading();

	LoadingView getCurrentLoadingView();
}
