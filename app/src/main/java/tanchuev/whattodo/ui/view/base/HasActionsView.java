package tanchuev.whattodo.ui.view.base;

/**
 * @author tanchuev
 */
public interface HasActionsView extends View {
	boolean hasActionView(Class<? extends ActionView> actionViewClass);

	ErrorView getErrorView();

	LoadingView getCurrentLoadingView();

	EmptyContentView getEmptyContentView();

	NoInternetView getNoInternetView();
}
