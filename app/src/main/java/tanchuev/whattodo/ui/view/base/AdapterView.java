package tanchuev.whattodo.ui.view.base;

import tanchuev.whattodo.ui.adapter.base.BaseAdapter;

public interface AdapterView<TAdapter extends BaseAdapter> extends HasActionsView {

	String RESULT_ITEM_SELECTED = "ResultItemSelected";

	TAdapter getAdapter();
}