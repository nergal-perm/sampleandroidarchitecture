package tanchuev.whattodo.ui.view.base;

/**
 * @author tanchuev
 */
public interface NoInternetView extends ActionView {
	void showNoInternet();

	void hideNoInternet();

	NoInternetView getNoInternetView();
}
