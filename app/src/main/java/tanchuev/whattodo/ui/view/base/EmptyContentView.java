package tanchuev.whattodo.ui.view.base;

/**
 * @author tanchuev
 */
public interface EmptyContentView extends ActionView {
	void showNoContent();

	void hideNoContent();

	EmptyContentView getEmptyContentView();
}
