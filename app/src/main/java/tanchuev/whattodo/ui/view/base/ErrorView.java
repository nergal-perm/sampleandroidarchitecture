package tanchuev.whattodo.ui.view.base;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

/**
 * @author tanchuev
 */
public interface ErrorView extends ActionView {

	//todo ErrorView getErrorView()

	void showError(@NonNull String message);

	void showError(@StringRes int message);

	void hideError();

	ErrorView getErrorView();
}
