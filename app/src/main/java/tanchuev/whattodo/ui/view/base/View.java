package tanchuev.whattodo.ui.view.base;

import tanchuev.whattodo.ui.activity.base.InjectorActivity;

/**
 * @author tanchuev
 */
public interface View {

	InjectorActivity getBaseActivity();

}
