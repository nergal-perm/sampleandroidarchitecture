package tanchuev.whattodo.ui.view;

import android.support.annotation.IdRes;

import tanchuev.whattodo.model.Employment;
import tanchuev.whattodo.ui.view.base.HasActionsView;

/**
 * @author tanchuev
 */

public interface EmploymentView extends HasActionsView {

	void setLikeState(boolean isLike, long likeCount);

	void clearView();

	void setEmployment(Employment employment);

	void setCurrentLoadingView(@IdRes int currentLoadingView);

}
