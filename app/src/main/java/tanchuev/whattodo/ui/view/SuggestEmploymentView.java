package tanchuev.whattodo.ui.view;

import tanchuev.whattodo.ui.view.base.HasActionsView;

/**
 * @author tanchuev
 */

public interface SuggestEmploymentView extends HasActionsView {
	void onSuccessfulSuggest();
}
