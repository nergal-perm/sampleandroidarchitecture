package tanchuev.whattodo.ui.adapter.viewholder;

import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import tanchuev.whattodo.R;
import tanchuev.whattodo.model.base.ModelName;

/**
 * @author tanchuev
 */

public class CheckableListViewHolder<T extends ModelName> extends RecyclerView.ViewHolder {

	@BindView(R.id.tvItemName)
	TextView tvItemName;

	@BindView(R.id.checkBox)
	AppCompatCheckBox checkBox;

	private Set<T> mCheckedItems;
	private T mItem;

	public CheckableListViewHolder(View itemView) {
		super(itemView);
		ButterKnife.bind(this, itemView);
	}

	public void bind(T item, Set<T> checkedItems) {
		mItem = item;
		mCheckedItems = checkedItems;

		tvItemName.setText(item.getName());

		if (checkedItems.contains(item)) {
			checkBox.setChecked(true);
		}
	}

	@OnClick(R.id.itemView)
	void onItemViewClick() {
		if (checkBox.isChecked()) {
			checkBox.setChecked(false);
		} else {
			checkBox.setChecked(true);
		}
	}

	@OnCheckedChanged(R.id.checkBox)
	void onCheckedChanged(boolean isChecked) {
		if (isChecked) {
			mCheckedItems.add(mItem);
		} else {
			mCheckedItems.remove(mItem);
		}
	}
}
