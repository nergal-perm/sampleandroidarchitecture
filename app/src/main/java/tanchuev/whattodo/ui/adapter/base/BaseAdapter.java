package tanchuev.whattodo.ui.adapter.base;

import java.util.List;


/**
 * @author tanchuev
 */

public interface BaseAdapter<T> {

	void add(T item);

	void addAll(List<T> itemList);

	void remove(int position);

	void remove(String id);

	void removeAll();

}