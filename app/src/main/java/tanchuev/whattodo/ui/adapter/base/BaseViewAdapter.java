package tanchuev.whattodo.ui.adapter.base;

import android.support.v4.app.Fragment;

import java.util.List;

import tanchuev.whattodo.ui.presenter.base.Presenter;

/**
 * @author tanchuev
 */

public abstract class BaseViewAdapter<TItem, TPresenter extends Presenter> extends Fragment {

	protected TPresenter mPresenter;

	abstract public void add(TItem item);

	abstract public void addAll(List<TItem> itemList);

	abstract public void remove(int position);

	abstract public void removeAll();
}
