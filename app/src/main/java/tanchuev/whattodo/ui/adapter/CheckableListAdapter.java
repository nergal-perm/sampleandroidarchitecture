package tanchuev.whattodo.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tanchuev.whattodo.R;
import tanchuev.whattodo.model.base.ModelName;
import tanchuev.whattodo.ui.adapter.base.recyclerview.ListPresenterAdapter;
import tanchuev.whattodo.ui.adapter.viewholder.CheckableListViewHolder;
import tanchuev.whattodo.ui.presenter.CheckableListPresenter;


public class CheckableListAdapter<T extends ModelName>
		extends ListPresenterAdapter<T, CheckableListPresenter, CheckableListViewHolder<T>> {

	private Set<T> mCheckedItems = new HashSet<>(); // список с выделенными итемами

	public CheckableListAdapter(CheckableListPresenter<T> presenter) {
		super(presenter);
	}

	@Override
	public CheckableListViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.li_checkable, parent, false);

		return new CheckableListViewHolder<>(v);
	}

	@Override
	public void onBindViewHolder(final CheckableListViewHolder<T> holder, final int position) {
		final T item = mItems.get(position);

		holder.bind(item, mCheckedItems);
	}

	public List<T> getCheckedItems() {
		return new ArrayList<>(mCheckedItems);
	}

	public void setCheckedItems(List<T> checkedItems) {
		mCheckedItems.clear();
		mCheckedItems.addAll(checkedItems);
	}
}

