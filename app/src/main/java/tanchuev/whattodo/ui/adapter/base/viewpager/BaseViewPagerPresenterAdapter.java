package tanchuev.whattodo.ui.adapter.base.viewpager;

import android.support.v4.view.PagerAdapter;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import tanchuev.whattodo.ui.adapter.base.BaseAdapter;
import tanchuev.whattodo.ui.presenter.base.Presenter;

/**
 * @author tanchuev
 */

public abstract class BaseViewPagerPresenterAdapter<TItem, TPresenter extends Presenter>
		extends PagerAdapter
		implements BaseAdapter<TItem> {


	protected List<TItem> mItems = new ArrayList<>();

	protected TPresenter mPresenter;

	public BaseViewPagerPresenterAdapter(TPresenter presenter) {
		mPresenter = presenter;
	}


	@Override
	public void add(TItem item) {
		if (mItems.contains(item))
			mItems.set(mItems.indexOf(item), item);
		else
			mItems.add(item);
		notifyDataSetChanged();
	}

	@Override
	public void addAll(List<TItem> items) {
		mItems.addAll(items);
		notifyDataSetChanged();
	}

	@Override
	public void remove(int position) {
		mItems.remove(position);
		notifyDataSetChanged();
	}

	@Override
	public void removeAll() {
		mItems.clear();
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mItems.size();
	}

	public abstract boolean isViewFromObject(View view, Object object);


}
