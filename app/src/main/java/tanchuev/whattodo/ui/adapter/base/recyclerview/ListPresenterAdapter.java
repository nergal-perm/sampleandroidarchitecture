package tanchuev.whattodo.ui.adapter.base.recyclerview;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import tanchuev.whattodo.model.base.Model;
import tanchuev.whattodo.ui.adapter.base.BaseAdapter;
import tanchuev.whattodo.ui.presenter.base.Presenter;

/**
 * @author tanchuev
 */
public abstract class ListPresenterAdapter<
		T extends Model,
		TPresenter extends Presenter,
		TViewHolder extends RecyclerView.ViewHolder>
		extends RecyclerView.Adapter<TViewHolder>
		implements BaseAdapter<T> {

	protected List<T> mItems = new ArrayList<>();

	protected TPresenter mPresenter;

	public ListPresenterAdapter(TPresenter presenter) {
		mPresenter = presenter;
	}

	@Override
	public void add(T item) {
		if (mItems.contains(item))
			mItems.set(mItems.indexOf(item), item);
		else
			mItems.add(item);
		notifyItemChanged(mItems.indexOf(item));
	}

	@Override
	public void addAll(List<T> items) {
		mItems.addAll(items);
		notifyDataSetChanged();
	}

	@Override
	public void remove(int position) {
		mItems.remove(position);
		notifyDataSetChanged();
	}

	@Override
	public void remove(String id) {
		for (T item : mItems) {
			if (item.getId().equals(id)) {
				mItems.remove(item);
				break;
			}
		}
		notifyDataSetChanged();
	}

	@Override
	public void removeAll() {
		mItems.clear();
		notifyDataSetChanged();
	}

	@Override
	public int getItemCount() {
		return mItems.size();
	}
}
