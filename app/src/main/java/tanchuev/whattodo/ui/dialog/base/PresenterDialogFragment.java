package tanchuev.whattodo.ui.dialog.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import tanchuev.whattodo.ui.presenter.base.Presenter;
import tanchuev.whattodo.ui.view.base.HasActionsView;

/**
 * @author tanchuev
 */
abstract class PresenterDialogFragment<
		Component,
		TPresenter extends Presenter>
		extends InjectorDialogFragment<Component>
		implements HasActionsView {

	@Inject
	protected TPresenter mPresenter;

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		mPresenter.attachView();
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	protected final void init() {
		super.init();
		getTypedPresenter().setView(this);
	}

	@Override
	public void onResume() {
		super.onResume();
		mPresenter.attachView();
	}

	@Override
	public void onPause() {
		super.onPause();
		mPresenter.detachView();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mPresenter.destroy();
	}

	private <TView extends HasActionsView> Presenter<TView> getTypedPresenter() {
		return mPresenter;
	}
}