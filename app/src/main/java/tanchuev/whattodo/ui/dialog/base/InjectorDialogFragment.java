package tanchuev.whattodo.ui.dialog.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.ButterKnife;
import tanchuev.whattodo.di.injector.Injector;
import tanchuev.whattodo.navigation.NavigatorImpl;
import tanchuev.whattodo.ui.activity.base.InjectorActivity;
import tanchuev.whattodo.ui.view.base.View;


/**
 * @author tanchuev
 */
public abstract class InjectorDialogFragment<Component>
		extends DialogFragment
		implements View, Injector<Component> {

	@Inject
	protected NavigatorImpl mNavigator;

	private Component mComponent;

	@CallSuper
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init();
	}

	@CallSuper
	@Override
	public android.view.View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		android.view.View v = inflater.inflate(getLayout(), container, false);
		ButterKnife.bind(this, v);
		return v;
	}

	@LayoutRes
	protected abstract int getLayout();

	@CallSuper
	protected void init() {
		initComponent();
		initInjections();
	}

	@Override
	public Component initComponent() {
		// by default activity component == fragment component
		// but we can override this method for init our fragment component,
		// which != activity component, but must extends activity component
		return mComponent = getBaseActivity().getComponent();
	}

	@Override
	public Component getComponent() {
		return mComponent;
	}

	@Override
	public InjectorActivity<Component> getBaseActivity() {
		return (InjectorActivity<Component>) getActivity();
	}
}