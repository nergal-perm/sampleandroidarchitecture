package tanchuev.whattodo.ui.dialog;

import org.greenrobot.eventbus.EventBus;

import butterknife.OnClick;
import tanchuev.whattodo.R;
import tanchuev.whattodo.event.SelectCategoriesEvent;
import tanchuev.whattodo.model.Category;

/**
 * @author tanchuev
 */

public class CategoryListDialogFragment extends CheckableListDialogFragment<Category> {

	@OnClick(R.id.btnOk)
	void onOkClick() {
		EventBus.getDefault().post(new SelectCategoriesEvent(mCheckableListAdapter.getCheckedItems()));
		dismiss();
	}

	@Override
	public void initInjections() {
		getComponent().injectInto(this);
	}
}
