package tanchuev.whattodo.ui.dialog;

import org.greenrobot.eventbus.EventBus;

import butterknife.OnClick;
import tanchuev.whattodo.R;
import tanchuev.whattodo.event.SelectLocationsEvent;
import tanchuev.whattodo.model.Location;

/**
 * @author tanchuev
 */

public class LocationListDialogFragment extends CheckableListDialogFragment<Location> {

	@OnClick(R.id.btnOk)
	void onOkClick() {
		EventBus.getDefault().post(new SelectLocationsEvent(mCheckableListAdapter.getCheckedItems()));
		dismiss();
	}

	@Override
	public void initInjections() {
		getComponent().injectInto(this);
	}
}
