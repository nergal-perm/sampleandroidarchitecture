package tanchuev.whattodo.ui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import butterknife.BindView;
import butterknife.OnClick;
import tanchuev.whattodo.R;
import tanchuev.whattodo.di.component.MainComponent;
import tanchuev.whattodo.model.base.ModelName;
import tanchuev.whattodo.ui.adapter.CheckableListAdapter;
import tanchuev.whattodo.ui.dialog.base.ActionsDialogFragment;
import tanchuev.whattodo.ui.presenter.CheckableListPresenter;
import tanchuev.whattodo.ui.view.CheckableListView;
import tanchuev.whattodo.ui.view.base.EmptyContentView;
import tanchuev.whattodo.ui.view.base.ErrorView;
import tanchuev.whattodo.ui.view.base.LoadingView;
import tanchuev.whattodo.ui.view.base.NoInternetView;
import tanchuev.whattodo.ui.widget.ProgressBar;
import tanchuev.whattodo.ui.widget.ToastView;

/**
 * @author tanchuev
 */

public abstract class CheckableListDialogFragment<T extends ModelName>
		extends ActionsDialogFragment<MainComponent, CheckableListPresenter<T>>
		implements CheckableListView {

	@BindView(R.id.recyclerView)
	RecyclerView recyclerView;
	@BindView(R.id.mainLoadingView)
	ProgressBar loadingView;

	protected CheckableListAdapter<T> mCheckableListAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		//getDialog().getWindow().setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.color.colorPrimary));
		getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		loadingView.setAnotherViews(recyclerView);

		if (mCheckableListAdapter == null) {
			mCheckableListAdapter = new CheckableListAdapter<>(mPresenter);
		}
		recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		recyclerView.setAdapter(mCheckableListAdapter);
		mPresenter.getItems();
	}

	@OnClick(R.id.btnCancel)
	void onCancelClick() {
		dismiss();
	}

	@Override
	public CheckableListAdapter<T> getAdapter() {
		return mCheckableListAdapter;
	}

	@Override
	protected int getLayout() {
		return R.layout.d_fr_checkable_list;
	}

	@Override
	public EmptyContentView getEmptyContentView() {
		return null;
	}

	@Override
	public ErrorView getErrorView() {
		return new ToastView(getContext());
	}

	@Override
	public LoadingView getCurrentLoadingView() {
		return loadingView;
	}

	@Override
	public NoInternetView getNoInternetView() {
		return null;
	}
}
