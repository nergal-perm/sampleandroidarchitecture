package tanchuev.whattodo.ui.fragment.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import tanchuev.whattodo.R;
import tanchuev.whattodo.di.injector.Injector;
import tanchuev.whattodo.navigation.NavigatorImpl;
import tanchuev.whattodo.ui.activity.base.InjectorActivity;
import tanchuev.whattodo.ui.view.base.View;

/**
 * @author tanchuev
 */
public abstract class InjectorFragment<Component>
		extends Fragment
		implements View, Injector<Component> {

	@Inject
	protected NavigatorImpl mNavigator;

	@BindView(R.id.scrollViewImage)
	protected ScrollView scrollViewImage;
	@BindView(R.id.ivBackground)
	protected AppCompatImageView ivBackground;

	private Component mComponent;
	private static int mBackgroundWidth;
	private static int mBackgroundHeight;

	@Override
	public final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init();
	}

	@CallSuper
	@Override
	public android.view.View onCreateView(LayoutInflater inflater,
										  @Nullable ViewGroup container,
										  @Nullable Bundle savedInstanceState) {
		android.view.View v = inflater.inflate(getLayout(), container, false);
		ButterKnife.bind(this, v);

		scrollViewImage.setEnabled(false);
		scrollViewImage.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				int width;
				int height;
				if (mBackgroundWidth == 0 && mBackgroundHeight == 0) {
					mBackgroundWidth = width = scrollViewImage.getWidth();
					mBackgroundHeight = height = scrollViewImage.getHeight();
				} else {
					width = mBackgroundWidth;
					height = mBackgroundHeight;
				}

				Picasso.with(getContext())
						.load(R.drawable.bg_light1)
						.resize(width,
								height)
						.into(ivBackground);
				scrollViewImage.getViewTreeObserver().removeOnGlobalLayoutListener(this);
			}
		});
		return v;
	}

	@LayoutRes
	protected abstract int getLayout();

	@CallSuper
	protected void init() {
		initComponent();
		initInjections();
	}

	@Override
	public Component initComponent() {
		// by default our activity component == our fragment component
		// but we can override this method for init our fragment component,
		// which != our activity component, but must extends our activity component
		return mComponent = getBaseActivity().getComponent();
	}

	@Override
	public Component getComponent() {
		return mComponent;
	}

	@Override
	public InjectorActivity<Component> getBaseActivity() {
		return (InjectorActivity<Component>) getActivity();
	}
}
