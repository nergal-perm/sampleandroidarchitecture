/**
 * Copyright (C) 2014 android10.org. All rights reserved.
 *
 * @author tanchuev
 */
package tanchuev.whattodo.ui.fragment.base;

import java.util.ArrayList;
import java.util.List;

import tanchuev.whattodo.ui.presenter.base.Presenter;
import tanchuev.whattodo.ui.view.base.ActionView;
import tanchuev.whattodo.ui.view.base.EmptyContentView;
import tanchuev.whattodo.ui.view.base.ErrorView;
import tanchuev.whattodo.ui.view.base.LoadingView;
import tanchuev.whattodo.ui.view.base.NoInternetView;
import tanchuev.whattodo.utils.Utils;

/**
 * @author tanchuev
 */
public abstract class ActionsFragment<
		Component,
		TPresenter extends Presenter>
		extends PresenterFragment<Component, TPresenter> {

	private List<Class<?>> mActionsViews = new ArrayList<>();
	private LoadingView mCurrentLoadingView;

	private List<Class<?>> getActionViews() {
		if (Utils.isNullOrEmpty(mActionsViews)) {
			return mActionsViews = new ArrayList<Class<?>>() {{
				if (getErrorView() != null) {
					add(ErrorView.class);
				}
				if (getCurrentLoadingView() != null) {
					add(LoadingView.class);
				}
				if (getEmptyContentView() != null) {
					add(EmptyContentView.class);
				}
				if (getNoInternetView() != null) {
					add(NoInternetView.class);
				}
			}};
		} else {
			return mActionsViews;
		}
	}

	@Override
	public boolean hasActionView(Class<? extends ActionView> actionViewClass) {
		return getActionViews().contains(actionViewClass);
	}

	//region LoadingView

	@Override
	public LoadingView getCurrentLoadingView() {
		return mCurrentLoadingView;
	}

	protected void setCurrentLoadingView(LoadingView currentLoadingView) {
		mCurrentLoadingView = currentLoadingView;
	}

	//endregion LoadingView
}