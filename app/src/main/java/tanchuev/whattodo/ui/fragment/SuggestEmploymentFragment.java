package tanchuev.whattodo.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.widget.Button;
import android.widget.EditText;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;
import tanchuev.whattodo.R;
import tanchuev.whattodo.di.component.MainComponent;
import tanchuev.whattodo.event.SelectCategoriesEvent;
import tanchuev.whattodo.event.SelectLocationsEvent;
import tanchuev.whattodo.model.Employment;
import tanchuev.whattodo.ui.fragment.base.ToolbarActionsFragment;
import tanchuev.whattodo.ui.presenter.SuggestEmploymentPresenter;
import tanchuev.whattodo.ui.view.SuggestEmploymentView;
import tanchuev.whattodo.ui.view.base.EmptyContentView;
import tanchuev.whattodo.ui.view.base.ErrorView;
import tanchuev.whattodo.ui.view.base.NoInternetView;
import tanchuev.whattodo.ui.widget.ProgressBar;
import tanchuev.whattodo.ui.widget.ToastView;
import tanchuev.whattodo.utils.Utils;
import tanchuev.whattodo.utils.ViewUtils;

/**
 * @author tanchuev
 */

public class SuggestEmploymentFragment
		extends ToolbarActionsFragment<MainComponent, SuggestEmploymentPresenter>
		implements SuggestEmploymentView {

	@BindView(R.id.buttonCategoryPick)
	Button buttonCategoryPick;
	@BindView(R.id.buttonLocationPick)
	Button buttonLocationPick;
	@BindView(R.id.etCostMin)
	EditText etCostMin;
	@BindView(R.id.etCostMax)
	EditText etCostMax;
	@BindView(R.id.etPersonMin)
	EditText etPersonMin;
	@BindView(R.id.etPersonMax)
	EditText etPersonMax;
	@BindView(R.id.etEmploymentName)
	EditText etEmploymentName;
	@BindView(R.id.etDescriptionEmployment)
	EditText etDescriptionEmployment;
	@BindView(R.id.mainLoadingView)
	ProgressBar mainLoadingView;
	@BindView(R.id.buttonSuggestEmployment)
	Button buttonSuggestEmployment;

	private Employment mEmployment = new Employment();

	public static SuggestEmploymentFragment newInstance() {
		return new SuggestEmploymentFragment();
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mainLoadingView.setAnotherViews(buttonSuggestEmployment);
		setCurrentLoadingView(mainLoadingView);
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onSelectCategoriesEvent(SelectCategoriesEvent event) {
		buttonCategoryPick.setText("");
		mEmployment.setCategories(event.getCategories());
		ViewUtils.setValuesToView(mEmployment.getCategories(), buttonCategoryPick, ", ");
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onSelectLocationsEvent(SelectLocationsEvent event) {
		buttonLocationPick.setText("");
		mEmployment.setLocations(event.getLocations());
		ViewUtils.setValuesToView(mEmployment.getLocations(), buttonLocationPick, ", ");
	}

	@OnClick(R.id.buttonCategoryPick)
	void onCategoryPickButtonClick() {
		mPresenter.pickCategory(mEmployment.getCategories());
	}

	@OnClick(R.id.buttonLocationPick)
	void onLocationPickButtonClick() {
		mPresenter.pickLocation(mEmployment.getLocations());
	}

	@OnClick(R.id.buttonSuggestEmployment)
	void onSuggestEmploymentButtonClick() {
		try {
			if (mEmployment.getCategories().isEmpty()) {
				throw new IllegalArgumentException(getString(R.string.errorInvalidCategoriesCount));
			}

			if (mEmployment.getLocations().isEmpty()) {
				throw new IllegalArgumentException(getString(R.string.errorInvalidLocationsCount));
			}

			mEmployment.setName(getStringValue(
					R.string.errorInvalidName,
					etEmploymentName.getText()));

			mEmployment.setMinCost(getIntValue(
					R.string.errorInvalidMinCost,
					etCostMin.getText()));

			mEmployment.setMaxCost(getIntValue(
					R.string.errorInvalidMaxCost,
					etCostMax.getText()));

			if (mEmployment.getMinCost() > mEmployment.getMaxCost()) {
				throw new IllegalArgumentException(getString(R.string.errorMinCostBiggerThanMaxCost));
			}

			if (mEmployment.getMinPersonCount() > mEmployment.getMaxPersonCount()) {
				throw new IllegalArgumentException(getString(R.string.errorMinPersonBiggerThanMaxPerson));
			}

			mEmployment.setMinPersonCount(getIntValue(
					R.string.errorInvalidMinPerson,
					etPersonMin.getText()));

			mEmployment.setMaxPersonCount(getIntValue(
					R.string.errorInvalidMaxPerson,
					etPersonMax.getText()));

			mEmployment.setFullDescription(getStringValue(
					R.string.errorInvalidDescription,
					etDescriptionEmployment.getText()));

			mPresenter.createEmployment(mEmployment);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			getErrorView().showError(e.getMessage());
		}
	}

	@Override
	public void onSuccessfulSuggest() {
		new ToastView(getContext()).showError(R.string.successfulSuggest);
		mNavigator.back();
	}

	private int getIntValue(@StringRes int error, CharSequence value)
			throws IllegalArgumentException {
		if (Utils.isNullOrEmpty(value)) {
			throw new IllegalArgumentException(getString(error));
		} else {
			return Integer.parseInt(value.toString());
		}
	}

	private String getStringValue(@StringRes int error, CharSequence value)
			throws IllegalArgumentException {
		if (Utils.isNullOrEmpty(value)) {
			throw new IllegalArgumentException(getString(error));
		} else {
			return value.toString();
		}
	}

	@Override
	protected int getLayout() {
		return R.layout.fr_suggest_employment;
	}

	@Override
	protected int getTitle() {
		return R.string.suggestEmployment;
	}

	@Override
	protected boolean hasBackButton() {
		return true;
	}

	@Override
	public ErrorView getErrorView() {
		return new ToastView(getContext());
	}

	@Override
	public EmptyContentView getEmptyContentView() {
		return null;
	}

	@Override
	public NoInternetView getNoInternetView() {
		return null;
	}

	@Override
	public void initInjections() {
		getComponent().injectInto(this);
	}
}