package tanchuev.whattodo.ui.fragment;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;
import tanchuev.whattodo.R;
import tanchuev.whattodo.di.component.MainComponent;
import tanchuev.whattodo.event.SelectCategoriesEvent;
import tanchuev.whattodo.event.SelectLocationsEvent;
import tanchuev.whattodo.model.Content;
import tanchuev.whattodo.model.Employment;
import tanchuev.whattodo.model.Filter;
import tanchuev.whattodo.ui.activity.AuthActivity;
import tanchuev.whattodo.ui.fragment.base.ToolbarActionsFragment;
import tanchuev.whattodo.ui.presenter.EmploymentPresenter;
import tanchuev.whattodo.ui.view.EmploymentView;
import tanchuev.whattodo.ui.view.base.EmptyContentView;
import tanchuev.whattodo.ui.view.base.ErrorView;
import tanchuev.whattodo.ui.view.base.NoInternetView;
import tanchuev.whattodo.ui.widget.ProgressBar;
import tanchuev.whattodo.ui.widget.SimplePlaceholderView;
import tanchuev.whattodo.ui.widget.ToastView;
import tanchuev.whattodo.utils.Utils;
import tanchuev.whattodo.utils.ViewUtils;

/**
 * @author tanchuev
 */

public class EmploymentFragment
		extends ToolbarActionsFragment<MainComponent, EmploymentPresenter>
		implements EmploymentView {

	@BindView(R.id.ivContent)
	ImageView ivContent;
	@BindView(R.id.tvEmploymentName)
	TextView tvEmploymentName;
	@BindView(R.id.tvEmploymentDescription)
	TextView tvEmploymentDescription;
	@BindView(R.id.tvCategories)
	TextView tvCategories;
	@BindView(R.id.tvLocations)
	TextView tvLocations;
	@BindView(R.id.tvCostFrom)
	TextView tvCostFrom;
	@BindView(R.id.tvCostTo)
	TextView tvCostTo;
	@BindView(R.id.tvPersonsFrom)
	TextView tvPersonsFrom;
	@BindView(R.id.tvPersonsTo)
	TextView tvPersonsTo;
	@BindView(R.id.buttonLike)
	Button buttonLike;
	@BindView(R.id.mainLayout)
	RelativeLayout mainLayout;
	@BindView(R.id.emptyView)
	SimplePlaceholderView emptyView;
	@BindView(R.id.noInternetView)
	SimplePlaceholderView noInternetView;
	@BindView(R.id.mainLoadingView)
	ProgressBar mainLoadingView;
	@BindView(R.id.likeLoadingView)
	ProgressBar likeLoadingView;

	@BindView(R.id.darkFilterBackground)
	View darkFilterBackground;
	@BindView(R.id.collapseLayout)
	LinearLayout collapseLayout;
	@BindView(R.id.buttonCategoryPick)
	Button buttonCategoryPick;
	@BindView(R.id.buttonLocationPick)
	Button buttonLocationPick;
	@BindView(R.id.buttonResetFilter)
	Button buttonResetFilter;
	@BindView(R.id.buttonApplyFilter)
	Button buttonApplyFilter;
	@BindView(R.id.etCostMin)
	EditText etCostFromPick;
	@BindView(R.id.etCostMax)
	EditText etCostToPick;
	@BindView(R.id.etPersonMin)
	EditText etPersonFromPick;
	@BindView(R.id.etPersonMax)
	EditText etPersonToPick;

	private Filter mFilter = new Filter();

	private ViewTreeObserver.OnGlobalLayoutListener mOnGlobalLayoutListener =
			new ViewTreeObserver.OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					collapseLayout.setTranslationY(-collapseLayout.getHeight());
					ViewUtils.removeOnGlobalLayoutListener(collapseLayout, this);
				}
			};

	public static EmploymentFragment newInstance() {
		return new EmploymentFragment();
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		collapseLayout.getViewTreeObserver()
				.addOnGlobalLayoutListener(mOnGlobalLayoutListener);

		emptyView.setAnotherViews(mainLayout, noInternetView, mainLoadingView);
		emptyView.setButtonClickListener(v -> onNextButtonClick());

		noInternetView.setAnotherViews(mainLayout, emptyView, mainLoadingView);
		noInternetView.setButtonClickListener(v -> onNextButtonClick());

		likeLoadingView.setAnotherViews(buttonLike);
		mainLoadingView.setAnotherViews(mainLayout, emptyView, noInternetView);
		setCurrentLoadingView(mainLoadingView);

		mPresenter.getEmployment();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_employment, menu);

		MenuItem menuItem = menu.findItem(R.id.actionFilter);
		if (menuItem != null) {
			updateFilterIcon(menuItem);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.actionFilter:
				item.setChecked(!item.isChecked());
				if (item.isChecked()) {
					openFilterView();
				} else {
					closeFilterView();
				}
				break;
			case R.id.actionSuggestEmployment:
				mNavigator.replaceFragment(SuggestEmploymentFragment.newInstance());
				break;
			case R.id.actionLogout:
				mNavigator.startActivity(AuthActivity.class, true);
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void setLikeState(boolean isLike, long likeCount) {
		buttonLike.setText(String.valueOf(likeCount));
		if (isLike)
			buttonLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like_on, 0, 0, 0);
		else {
			buttonLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like_off, 0, 0, 0);
		}
	}

	public void updateFilterIcon(MenuItem item) {
		if (!mFilter.isEmpty()) {
			item.setIcon(R.drawable.ic_filter_on);
		} else {
			item.setIcon(R.drawable.ic_filter_off);
		}
		item.setChecked(!(collapseLayout.getVisibility() == View.VISIBLE));
	}

	@Override
	public void setEmployment(Employment employment) {
		ViewUtils.setValuesToView(employment.getCategories(), tvCategories, ", ");
		ViewUtils.setValuesToView(employment.getLocations(), tvLocations, ", ");

		tvCostFrom.setText(String.valueOf(employment.getMinCost()));
		tvCostTo.setText(employment.getMaxCost() == 0 ? "" : String.valueOf(employment.getMaxCost()));
		tvPersonsFrom.setText(String.valueOf(employment.getMinPersonCount()));
		tvPersonsTo.setText(employment.getMaxPersonCount() == 0 ? getString(R.string.andMore) : String.valueOf(employment.getMaxPersonCount()));
		tvEmploymentName.setText(employment.getName());
		tvEmploymentDescription.setText(employment.getFullDescription());
		setLikeState(employment.isLikedByUser(), employment.getRating());

		Content content = employment.getContent();

		if (content != null) {
			ivContent.setVisibility(View.VISIBLE);
			if (content.getContentType() == Content.ContentType.Photo) {
				Picasso.with(getActivity())
						.load(content.getContentUrl())
						.placeholder(R.drawable.d_placeholder)
						.fit()
						.centerInside()
						//.doOnError(getContext().getDrawable(R.drawable.ic_clear_black_24dp))
						.into(ivContent);
			}
		}

	}

	@Override
	public void setCurrentLoadingView(@IdRes int currentLoadingView) {
		switch (currentLoadingView) {
			case R.id.mainLoadingView:
				super.setCurrentLoadingView(mainLoadingView);
				break;
			case R.id.likeLoadingView:
			default:
				super.setCurrentLoadingView(likeLoadingView);
				break;
		}
	}

	@Override
	public void clearView() {
		tvCategories.setText("");
		tvLocations.setText("");
		tvCostFrom.setText("");
		tvCostTo.setText("");
		tvPersonsFrom.setText("");
		tvPersonsTo.setText("");
		ivContent.setVisibility(View.GONE);
	}

	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onSelectCategoriesEvent(SelectCategoriesEvent event) {
		buttonCategoryPick.setText("");
		mFilter.setCategories(event.getCategories());
		ViewUtils.setValuesToView(mFilter.getCategories(), buttonCategoryPick, ", ");
	}

	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onSelectLocationsEvent(SelectLocationsEvent event) {
		buttonLocationPick.setText("");
		mFilter.setLocations(event.getLocations());
		ViewUtils.setValuesToView(mFilter.getLocations(), buttonLocationPick, ", ");
	}

	@OnClick(R.id.buttonCategoryPick)
	void onCategoryPickButtonClick() {
		mPresenter.pickCategory(mFilter.getCategories());
	}

	@OnClick(R.id.buttonLocationPick)
	void onLocationPickButtonClick() {
		mPresenter.pickLocation(mFilter.getLocations());
	}

	@OnClick(R.id.buttonLikeLayout)
	void onLikeClick() {
		mPresenter.like();
	}

	@OnClick(R.id.buttonNext)
	void onNextButtonClick() {
		if (mFilter.isEmpty()) {
			mPresenter.getEmployment();
		} else {
			mPresenter.getEmployment(mFilter);
		}
	}

	@OnClick(R.id.buttonResetFilter)
	void onResetFilterButtonClick() {
		buttonCategoryPick.setText("");
		buttonLocationPick.setText("");
		etCostFromPick.setText("");
		etCostToPick.setText("");
		etPersonFromPick.setText("");
		etPersonToPick.setText("");
		if (!mFilter.isEmpty()) {
			mFilter = new Filter();
			mPresenter.getEmployment();
		}
		closeFilterView();
	}

	@OnClick(R.id.buttonApplyFilter)
	void onApplyFilterButtonClick() {
		boolean fieldsEmpty = true;
		if (!Utils.isNullOrEmpty(etCostFromPick.getText().toString())) {
			fieldsEmpty = false;
			mFilter.setMinCost(Integer.parseInt(etCostFromPick.getText().toString()));
		}
		if (!Utils.isNullOrEmpty(etCostToPick.getText().toString())) {
			fieldsEmpty = false;
			mFilter.setMaxCost(Integer.parseInt(etCostToPick.getText().toString()));
		}
		if (!Utils.isNullOrEmpty(etPersonFromPick.getText().toString())) {
			fieldsEmpty = false;
			mFilter.setMinPersonCount(Integer.parseInt(etPersonFromPick.getText().toString()));
		}
		if (!Utils.isNullOrEmpty(etPersonToPick.getText().toString())) {
			fieldsEmpty = false;
			mFilter.setMaxPersonCount(Integer.parseInt(etPersonToPick.getText().toString()));
		}

		if ((!fieldsEmpty
				|| !Utils.isNullOrEmpty(mFilter.getCategories())
				|| !Utils.isNullOrEmpty(mFilter.getLocations()))
				&& !mFilter.isEmpty()) {
			mPresenter.getEmployment(mFilter);
			closeFilterView();
		} else {
			ViewUtils.showToastMessage(getContext(), R.string.errorEmptyFilter, Toast.LENGTH_SHORT);
		}
	}

	@OnClick(R.id.darkFilterBackground)
	void onDarkFilterBackgroundClick() {
		closeFilterView();
	}

	@OnClick(R.id.collapseLayout)
	void onCollapseLayoutClick() {
	}

	private void openFilterView() {
		collapseLayout.setVisibility(View.VISIBLE);
		collapseLayout.animate()
				.setDuration(700)
				.translationY(0)
				.withStartAction(() -> ViewUtils.setVisibility(View.VISIBLE, darkFilterBackground))
				.setUpdateListener(animation -> {
					if (animation.getCurrentPlayTime() != 0) {
						darkFilterBackground.setAlpha(
								(float) animation.getCurrentPlayTime() / (float) animation.getDuration());
					}
				});
	}

	private void closeFilterView() {
		ViewUtils.hideKeyboard(getActivity());
		getActivity().supportInvalidateOptionsMenu();
		collapseLayout.animate()
				.setDuration(700)
				.translationY(-collapseLayout.getMeasuredHeight())
				.setUpdateListener(animation -> {
					if (animation.getCurrentPlayTime() != 0) {
						darkFilterBackground.setAlpha(
								1 - (float) animation.getCurrentPlayTime() / (float) animation.getDuration());
					}
				})
				.withEndAction(
						() -> {
							ViewUtils.setVisibility(View.GONE, darkFilterBackground);
							collapseLayout.setVisibility(View.GONE);
						});
	}

	@Override
	protected int getLayout() {
		return R.layout.fr_employment;
	}

	@Override
	protected int getTitle() {
		return R.string.app_name;
	}

	@Override
	public ErrorView getErrorView() {
		return new ToastView(getContext());
	}

	@Override
	public EmptyContentView getEmptyContentView() {
		return emptyView;
	}

	@Override
	public NoInternetView getNoInternetView() {
		return noInternetView;
	}

	@Override
	public void initInjections() {
		getComponent().injectInto(this);
	}
}
