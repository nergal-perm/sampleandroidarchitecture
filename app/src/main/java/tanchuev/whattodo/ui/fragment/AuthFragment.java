package tanchuev.whattodo.ui.fragment;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;
import tanchuev.whattodo.R;
import tanchuev.whattodo.di.component.AuthComponent;
import tanchuev.whattodo.ui.activity.MainActivity;
import tanchuev.whattodo.ui.fragment.base.ToolbarActionsFragment;
import tanchuev.whattodo.ui.presenter.AuthPresenter;
import tanchuev.whattodo.ui.view.AuthView;
import tanchuev.whattodo.ui.view.base.EmptyContentView;
import tanchuev.whattodo.ui.view.base.ErrorView;
import tanchuev.whattodo.ui.view.base.LoadingView;
import tanchuev.whattodo.ui.view.base.NoInternetView;
import tanchuev.whattodo.utils.Utils;
import tanchuev.whattodo.utils.ViewUtils;

/**
 * @author tanchuev
 */

public class AuthFragment
		extends ToolbarActionsFragment<AuthComponent, AuthPresenter>
		implements AuthView {

	@BindView(R.id.etLogin)
	EditText etLogin;
	@BindView(R.id.etPassword)
	EditText etPassword;
	@BindView(R.id.buttonAuth)
	Button buttonAuth;
	@BindView(R.id.buttonEnterWithoutAuth)
	Button buttonEnterWithoutAuth;
	@BindView(R.id.buttonRegistration)
	Button buttonRegistration;

	public static AuthFragment newInstance() {
		return new AuthFragment();
	}

	@OnClick(R.id.buttonAuth)
	void onAuthButtonClick() {
		if (Utils.isNullOrEmpty(etLogin.getText().toString())
				&& Utils.isNullOrEmpty(etLogin.getText().toString())) {
			ViewUtils.showToastMessage(getActivity(),
					getString(R.string.errorEmptyParameters),
					Toast.LENGTH_SHORT);
		} else {
			mNavigator.startActivity(MainActivity.class, true);
		}
	}

	@OnClick(R.id.buttonEnterWithoutAuth)
	void onEnterWithoutAuthButtonClick() {
		mNavigator.startActivity(MainActivity.class, true);
	}

	@OnClick(R.id.buttonRegistration)
	void onRegistrationButtonClick() {
		mNavigator.replaceFragment(RegistrationFragment.newInstance());
	}

	@Override
	protected int getLayout() {
		return R.layout.fr_auth;
	}

	@Override
	protected int getTitle() {
		return R.string.auth;
	}

	@Override
	public ErrorView getErrorView() {
		return null;
	}

	@Override
	public LoadingView getCurrentLoadingView() {
		return null;
	}

	@Override
	public EmptyContentView getEmptyContentView() {
		return null;
	}

	@Override
	public NoInternetView getNoInternetView() {
		return null;
	}

	@Override
	public void initInjections() {
		getComponent().injectInto(this);
	}
}
