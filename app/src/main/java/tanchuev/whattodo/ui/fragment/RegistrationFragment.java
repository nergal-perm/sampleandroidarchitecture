package tanchuev.whattodo.ui.fragment;

import android.widget.EditText;

import butterknife.BindView;
import butterknife.OnClick;
import tanchuev.whattodo.R;
import tanchuev.whattodo.model.User;
import tanchuev.whattodo.di.component.AuthComponent;
import tanchuev.whattodo.ui.fragment.base.ToolbarActionsFragment;
import tanchuev.whattodo.ui.presenter.RegistrationPresenter;
import tanchuev.whattodo.ui.view.RegistrationView;
import tanchuev.whattodo.ui.view.base.EmptyContentView;
import tanchuev.whattodo.ui.view.base.ErrorView;
import tanchuev.whattodo.ui.view.base.LoadingView;
import tanchuev.whattodo.ui.view.base.NoInternetView;
import tanchuev.whattodo.ui.widget.ToastView;

/**
 * @author tanchuev
 */

public class RegistrationFragment
		extends ToolbarActionsFragment<AuthComponent, RegistrationPresenter>
		implements RegistrationView {

	@BindView(R.id.etLogin)
	EditText etLogin;
	@BindView(R.id.etPassword)
	EditText etPassword;
	@BindView(R.id.etRepeatPassword)
	EditText etRepeatPassword;
	@BindView(R.id.etFirstName)
	EditText etFirstName;
	@BindView(R.id.etLastName)
	EditText etLastName;
	@BindView(R.id.etPatronymic)
	EditText etPatronymic;
	@BindView(R.id.etEmail)
	EditText etEmail;

	public static RegistrationFragment newInstance() {
		return new RegistrationFragment();
	}

	@OnClick(R.id.buttonRegistration)
	void onRegistrationButtonClick() {
		//todo тут проверять на правильность пароли и выставлять им doOnError view, если они не совпадают,
		//todo тоже делать, если ошибки в других данных, придумать как выводить ошибки для конкретных полей
		if (etPassword.getText().toString().equals(etPassword.getText().toString())) {
			mPresenter.registration(new User());
		} else {
			getErrorView().showError("Пароли не совпадают!");
		}
	}

	@Override
	protected int getLayout() {
		return R.layout.fr_registration;
	}

	@Override
	protected boolean hasBackButton() {
		return true;
	}

	@Override
	protected int getTitle() {
		return R.string.registration;
	}

	@Override
	public ErrorView getErrorView() {
		return new ToastView(getActivity());
	}

	@Override
	public LoadingView getCurrentLoadingView() {
		return null;
	}

	@Override
	public EmptyContentView getEmptyContentView() {
		return null;
	}

	@Override
	public NoInternetView getNoInternetView() {
		return null;
	}

	@Override
	public void initInjections() {
		getComponent().injectInto(this);
	}
}
