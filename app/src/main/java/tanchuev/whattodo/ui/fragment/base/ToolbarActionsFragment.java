package tanchuev.whattodo.ui.fragment.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import butterknife.BindView;
import tanchuev.whattodo.R;
import tanchuev.whattodo.ui.presenter.base.Presenter;

/**
 * @author tanchuev
 */
public abstract class ToolbarActionsFragment<
		Component,
		TPresenter extends Presenter>
		extends ActionsFragment<Component, TPresenter> {

	@BindView(R.id.toolbar)
	Toolbar toolbar;

	@CallSuper
	@Override
	public android.view.View onCreateView(LayoutInflater inflater,
										  @Nullable ViewGroup container,
										  @Nullable Bundle savedInstanceState) {
		android.view.View v = super.onCreateView(inflater, container, savedInstanceState);
		getBaseActivity().setSupportActionBar(toolbar);
		toolbar.setOverflowIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_three_dots_white_24dp));

		if (hasBackButton()) {
			//getBaseActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			//getBaseActivity().getSupportActionBar().setDisplayShowHomeEnabled(true);
			toolbar.setNavigationIcon(getBackButton());
			toolbar.setNavigationOnClickListener(v1 -> {
				if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 1) {
					getActivity().getSupportFragmentManager().popBackStack();
				} else {
					getActivity().finish();
				}
			});
		}
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		if (getTitle() != -1) {
			setTitle(getString(getTitle()), getSubtitle());
		}
	}

	protected boolean hasBackButton() {
		return false;
	}

	@DrawableRes
	protected int getBackButton() {
		return R.drawable.ic_back_arrow;
	}

	protected void setTitle(@StringRes int titleResId) {
		setTitle(getString(titleResId), null);
	}

	protected void setTitle(@StringRes int title, @StringRes int subtitle) {
		setTitle(getString(title), getString(subtitle));
	}

	protected void setTitle(String title, String subtitle) {
		toolbar.setTitle(title);
		toolbar.setSubtitle(subtitle);
	}

	@StringRes
	protected int getTitle() {
		return -1;
	}

	protected String getSubtitle() {
		return null;
	}
}
