package tanchuev.whattodo.ui.activity.base;


import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;

import butterknife.BindView;
import tanchuev.whattodo.R;

/**
 * @author tanchuev
 */
public abstract class ToolbarActivity<TInjector> extends InjectorActivity<TInjector> {
	@BindView(R.id.toolbar)
	public Toolbar mToolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);

		mToolbar.setNavigationIcon(R.drawable.ic_back_arrow);
		mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
					getSupportFragmentManager().popBackStack();
				} else {
					finish();
				}
			}
		});
	}

	@Override
	@CallSuper
	protected void onResume() {
		super.onResume();

		mToolbar.setTitleTextColor(ActivityCompat.getColor(this, android.R.color.white));
		//mToolbar.setPopupTheme(R.style.ToolBarStyle);
		mToolbar.setOverflowIcon(ActivityCompat.getDrawable(this, R.drawable.ic_three_dots_white_24dp));
	}
}
