package tanchuev.whattodo.ui.activity.base;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;
import tanchuev.whattodo.AndroidApplication;
import tanchuev.whattodo.di.component.ApplicationComponent;
import tanchuev.whattodo.di.injector.Injector;
import tanchuev.whattodo.navigation.NavigatorImpl;

/**
 * @author tanchuev
 */
public abstract class InjectorActivity<Component>
		extends AppCompatActivity
		implements Injector<Component> {

	@Inject
	protected NavigatorImpl mNavigator;

	private Component mComponent;

	@CallSuper
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getLayout());
		ButterKnife.bind(this);
		init();
		if (getStartFragment() != null) {
			mNavigator.replaceFragment(getStartFragment());
		}
	}

	protected Fragment getStartFragment() {
		return null;
	}

	@LayoutRes
	protected abstract int getLayout();

	@Override
	public void onBackPressed() {
		FragmentManager fm = getSupportFragmentManager();
		if (fm.getBackStackEntryCount() == 1) {
			finish();
		} else {
			fm.popBackStack();
		}
	}

	@CallSuper
	protected void init() {
		mComponent = initComponent();
		initInjections();
	}

	@Override
	public Component getComponent() {
		return mComponent;
	}

	protected ApplicationComponent getApplicationComponent() {
		return ((AndroidApplication) getApplication()).getApplicationComponent();
	}
}
