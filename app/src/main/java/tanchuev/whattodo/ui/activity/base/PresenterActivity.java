package tanchuev.whattodo.ui.activity.base;

import android.os.Bundle;

import javax.inject.Inject;

import tanchuev.whattodo.ui.presenter.base.Presenter;
import tanchuev.whattodo.ui.view.base.HasActionsView;

/**
 * @author tanchuev
 */

public abstract class PresenterActivity<
		Component,
		TPresenter extends Presenter>
		extends InjectorActivity<Component>
		implements HasActionsView {

	@Inject
	protected TPresenter mPresenter;

	@Override
	public InjectorActivity getBaseActivity() {
		return this;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPresenter.attachView();
	}

	@Override
	protected final void init() {
		super.init();
		getTypedPresenter().setView(this);
	}

	protected TPresenter getPresenter() {
		return mPresenter;
	}

	@Override
	public void onResume() {
		super.onResume();
		mPresenter.attachView();
	}

	@Override
	public void onPause() {
		super.onPause();
		mPresenter.detachView();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mPresenter.destroy();
	}

	private <TView extends HasActionsView> Presenter<TView> getTypedPresenter() {
		return mPresenter;
	}
}