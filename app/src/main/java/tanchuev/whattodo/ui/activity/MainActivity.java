package tanchuev.whattodo.ui.activity;

import android.support.v4.app.Fragment;

import tanchuev.whattodo.R;
import tanchuev.whattodo.di.component.DaggerMainComponent;
import tanchuev.whattodo.di.component.MainComponent;
import tanchuev.whattodo.di.modules.ActivityModule;
import tanchuev.whattodo.ui.activity.base.InjectorActivity;
import tanchuev.whattodo.ui.fragment.EmploymentFragment;

public class MainActivity extends InjectorActivity<MainComponent> {

    @Override
    protected Fragment getStartFragment() {
        return EmploymentFragment.newInstance();
    }

    @Override
    protected int getLayout() {
        return R.layout.ac_main;
    }

    @Override
    public MainComponent initComponent() {
        return DaggerMainComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(new ActivityModule(this))
                .build();
    }

    @Override
    public void initInjections() {
        getComponent().injectInto(this);
    }
}