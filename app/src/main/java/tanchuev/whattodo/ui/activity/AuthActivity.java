package tanchuev.whattodo.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import tanchuev.whattodo.R;
import tanchuev.whattodo.di.component.AuthComponent;
import tanchuev.whattodo.di.component.DaggerAuthComponent;
import tanchuev.whattodo.di.modules.ActivityModule;
import tanchuev.whattodo.ui.activity.base.InjectorActivity;
import tanchuev.whattodo.ui.fragment.AuthFragment;

public class AuthActivity extends InjectorActivity<AuthComponent> {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected Fragment getStartFragment() {
		return AuthFragment.newInstance();
	}

	@Override
	protected int getLayout() {
		return R.layout.ac_fragment_container;
	}

	@Override
	public AuthComponent initComponent() {
		return DaggerAuthComponent.builder()
				.applicationComponent(getApplicationComponent())
				.activityModule(new ActivityModule(this))
				.build();
	}

	@Override
	public void initInjections() {
		getComponent().injectInto(this);
	}
}