package tanchuev.whattodo.ui.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import tanchuev.whattodo.R;
import tanchuev.whattodo.ui.view.base.ErrorView;
import tanchuev.whattodo.utils.Utils;

/**
 * @author tanchuev
 */
public class ToastView
		extends LinearLayout
		implements ErrorView {
	@BindView(R.id.tvToast)
	TextView tvToast;

	private Context mContext;

	public ToastView(Context context) {
		super(context);
		mContext = context;
		init(context);
	}

	private void init(Context context) {
		inflate(context, R.layout.l_toast_view, this);
		ButterKnife.bind(this);
	}

	@Override
	public void showError(@StringRes int message) {
		showError(mContext.getString(message));
	}

	@Override
	public void showError(@NonNull String message) {
		if (Utils.isNullOrEmpty(message)) {
			return;
		}

		tvToast.setText(message);
		Toast toast = new Toast(mContext.getApplicationContext());
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(this);
		toast.show();
	}

	@Override
	public void hideError() {

	}

	@Override
	public ErrorView getErrorView() {
		return this;
	}
}
