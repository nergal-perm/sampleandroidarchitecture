package tanchuev.whattodo.ui.widget;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.widget.Button;

import tanchuev.whattodo.R;

import static android.content.DialogInterface.BUTTON_NEGATIVE;
import static android.content.DialogInterface.BUTTON_POSITIVE;

public class DialogMessageBuilder extends AlertDialog.Builder {

	private boolean hasOnShowListener;

	public DialogMessageBuilder(@NonNull Context context) {
		super(context);
	}

	public DialogMessageBuilder(@NonNull Context context, int themeResId) {
		super(context, themeResId);
	}

	@Override
	public AlertDialog show() {
		AlertDialog dialog = create();
		if (!hasOnShowListener) {
			dialog.setOnShowListener(new DialogInterface.OnShowListener() {
				@Override
				public void onShow(DialogInterface dialogInterface) {
					Button negativeButton = dialog.getButton(BUTTON_NEGATIVE);
					Button positiveButton = dialog.getButton(BUTTON_POSITIVE);

					negativeButton.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.black));
					positiveButton.setBackgroundColor(Color.RED);
				}
			});
			hasOnShowListener = true;
		}
		dialog.show();
		return dialog;
	}
}