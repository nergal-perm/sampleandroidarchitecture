package tanchuev.whattodo.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import tanchuev.whattodo.R;
import tanchuev.whattodo.ui.view.base.ActionView;
import tanchuev.whattodo.ui.view.base.LoadingView;
import tanchuev.whattodo.utils.ViewUtils;

public class ProgressBar
		extends android.widget.ProgressBar
		implements LoadingView {

	private View[] mAnotherViews;

	public ProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}

	public ProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(context, attrs);
	}

	private void init(Context context, AttributeSet attrs) {
		TypedArray a = context.getTheme().obtainStyledAttributes(
				attrs,
				R.styleable.ProgressBar,
				0, 0);

		try {
			getIndeterminateDrawable().setColorFilter(
					a.getColor(R.styleable.ProgressBar_progressColor,
							ContextCompat.getColor(context, R.color.lightTransparent50)),
					PorterDuff.Mode.MULTIPLY);
		} finally {
			a.recycle();
		}
	}

	@Override
	public void showLoading() {
		ViewUtils.setVisibility(View.GONE, mAnotherViews);
		ViewUtils.setVisibility(View.VISIBLE, this);
	}

	@Override
	public void hideLoading() {
		for (View v : mAnotherViews) {
			if (!(v instanceof ActionView)) {
				ViewUtils.setVisibility(View.VISIBLE, v);
				break;
			}
		}
		ViewUtils.setVisibility(View.GONE, this);
	}

	@Override
	public LoadingView getCurrentLoadingView() {
		return this;
	}

	public void setAnotherViews(View... anotherViews) {
		mAnotherViews = anotherViews;
	}
}