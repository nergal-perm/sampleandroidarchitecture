package tanchuev.whattodo.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import tanchuev.whattodo.R;
import tanchuev.whattodo.ui.view.base.ActionView;
import tanchuev.whattodo.ui.view.base.EmptyContentView;
import tanchuev.whattodo.ui.view.base.NoInternetView;
import tanchuev.whattodo.utils.ViewUtils;

/**
 * @author tanchuev
 */

public class SimplePlaceholderView
		extends LinearLayout
		implements EmptyContentView, NoInternetView {

	@BindView(R.id.placeholderViewIcon)
	ImageView placeholderViewIcon;
	@BindView(R.id.placeholderViewText)
	TextView placeholderViewText;
	@BindView(R.id.placeholderViewButton)
	Button placeholderViewButton;

	private View[] mAnotherViews;

	public SimplePlaceholderView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}

	private void init(Context context, AttributeSet attrs) {
		inflate(context, R.layout.l_placeholder_view, this);
		ButterKnife.bind(this);

		TypedArray a = context.getTheme().obtainStyledAttributes(
				attrs,
				R.styleable.SimplePlaceholderView,
				0, 0);

		try {
			placeholderViewText.setText(a.getString(R.styleable.SimplePlaceholderView_text));
			placeholderViewIcon.setImageResource(a.getResourceId(R.styleable.SimplePlaceholderView_icon, R.drawable.ic_warning));
			String buttonText = a.getString(R.styleable.SimplePlaceholderView_buttonText);
			if (!TextUtils.isEmpty(buttonText)) {
				placeholderViewButton.setText(buttonText);
				placeholderViewButton.setVisibility(VISIBLE);
			} else {
				placeholderViewButton.setVisibility(GONE);
			}
		} finally {
			a.recycle();
		}
	}

	public void setButtonClickListener(OnClickListener buttonClickListener) {
		placeholderViewButton.setOnClickListener(buttonClickListener);
	}

	public void bindView(@DrawableRes int icon, @StringRes int text, @StringRes int buttonText, OnClickListener buttonClickListener) {
		placeholderViewIcon.setImageResource(icon);
		placeholderViewText.setText(text);
		ViewUtils.setVisibility(View.VISIBLE, placeholderViewButton);
		placeholderViewButton.setText(buttonText);
		placeholderViewButton.setOnClickListener(buttonClickListener);
	}

	public void bindView(@DrawableRes int icon, @StringRes int text) {
		placeholderViewIcon.setImageResource(icon);
		placeholderViewText.setText(text);
		ViewUtils.setVisibility(View.GONE, placeholderViewButton);
	}

	public void bindView(@StringRes int text) {
		placeholderViewText.setText(text);
		ViewUtils.setVisibility(View.GONE, placeholderViewIcon);
		ViewUtils.setVisibility(View.GONE, placeholderViewButton);
	}

	@Override
	public void showNoContent() {
		show();
	}

	@Override
	public void hideNoContent() {
		hide();
	}

	@Override
	public void showNoInternet() {
		show();
	}

	@Override
	public void hideNoInternet() {
		hide();
	}

	public void setAnotherViews(View... anotherViews) {
		mAnotherViews = anotherViews;
	}

	@Override
	public EmptyContentView getEmptyContentView() {
		return this;
	}

	@Override
	public NoInternetView getNoInternetView() {
		return this;
	}

	private void show() {
		ViewUtils.setVisibility(View.GONE, mAnotherViews);
		ViewUtils.setVisibility(View.VISIBLE, this);
	}

	private void hide() {
		for (View v : mAnotherViews) {
			if (!(v instanceof ActionView)) {
				ViewUtils.setVisibility(View.VISIBLE, v);
				break;
			}
		}
		ViewUtils.setVisibility(View.GONE, this);
	}
}
