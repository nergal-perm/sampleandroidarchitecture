package tanchuev.whattodo.model;

import java.util.ArrayList;
import java.util.List;

import tanchuev.whattodo.model.base.Model;
import tanchuev.whattodo.utils.Utils;

/**
 * @author tanchuev
 */

public class Filter extends Model<Filter> {
	private List<Category> mCategories = new ArrayList<>();
	private List<Location> mLocations = new ArrayList<>();
	private int mMinPersonCount;
	private int mMaxPersonCount;
	private int mMinCost;
	private int mMaxCost;

	private boolean isRandom; //переменная для определения как выводить занятия,
	// по рейтингу(от топовых к не топовым) или рандомно

	public List<Category> getCategories() {
		return mCategories;
	}

	public Filter setCategories(List<Category> categories) {
		mCategories = categories;
		return this;
	}

	public List<Location> getLocations() {
		return mLocations;
	}

	public Filter setLocations(List<Location> locations) {
		mLocations = locations;
		return this;
	}

	public int getMinPersonCount() {
		return mMinPersonCount;
	}

	public Filter setMinPersonCount(int minPersonCount) {
		mMinPersonCount = minPersonCount;
		return this;
	}

	public int getMaxPersonCount() {
		return mMaxPersonCount;
	}

	public Filter setMaxPersonCount(int maxPersonCount) {
		mMaxPersonCount = maxPersonCount;
		return this;
	}

	public int getMinCost() {
		return mMinCost;
	}

	public Filter setMinCost(int minCost) {
		mMinCost = minCost;
		return this;
	}

	public int getMaxCost() {
		return mMaxCost;
	}

	public Filter setMaxCost(int maxCost) {
		mMaxCost = maxCost;
		return this;
	}

	public boolean isRandom() {
		return isRandom;
	}

	public Filter setRandom(boolean random) {
		isRandom = random;
		return this;
	}

	public boolean isEmpty() {
		return Utils.isNullOrEmpty(mCategories) &&
				Utils.isNullOrEmpty(mLocations) &&
				mMinPersonCount == 0 &&
				mMaxPersonCount == 0 &&
				mMinCost == 0 &&
				mMaxCost == 0;
	}
}
