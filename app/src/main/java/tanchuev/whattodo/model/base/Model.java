package tanchuev.whattodo.model.base;

import android.support.annotation.NonNull;

import java.util.Date;
import java.util.UUID;

/**
 * @author tanchuev
 */
public abstract class Model<T extends Model> implements Comparable<T>, Cloneable {

	public static final String ID = "ID";

	private String mId;

	private Date mCreationDate;

	public Model() {
		mId = UUID.randomUUID().toString().toUpperCase();
		mCreationDate = new Date();
	}

	public String getId() {
		return mId;
	}

	public void setId(String id) {
		this.mId = id;
	}

	public Date getCreationDate() {
		return mCreationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.mCreationDate = creationDate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Model<?> that = (Model<?>) o;
		return mId != null ? mId.equals(that.mId) : that.mId == null;
	}

	@Override
	public int hashCode() {
		int result = mId != null ? mId.hashCode() : 0;
		result = 31 * result + (mId != null ? mId.hashCode() : 0);
		return result;
	}

	@Override
	public int compareTo(@NonNull T another) {
		int lastObject = this.getId().toUpperCase().compareTo(another.getId().toUpperCase());
		return lastObject == 0 ? this.getId().toUpperCase().compareTo(another.getId().toUpperCase()) : lastObject;
	}
}
