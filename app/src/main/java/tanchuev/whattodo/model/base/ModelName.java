package tanchuev.whattodo.model.base;


/**
 * @author tanchuev
 */
public class ModelName<M extends ModelName>
		extends Model<M> {

	private String mName;

	public String getName() {
		return mName;
	}

	public M setName(String name) {
		mName = name;
		return (M) this;
	}
}
