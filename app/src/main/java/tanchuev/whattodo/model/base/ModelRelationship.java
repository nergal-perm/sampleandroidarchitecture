package tanchuev.whattodo.model.base;


import java.util.List;

/**
 * @author tanchuev
 */
public class ModelRelationship<Key extends ModelRelationship, Value extends Model>
		extends ModelName<Key> {
	private List<Value> mValues;

	public List<Value> getValues() {
		return mValues;
	}

	public ModelRelationship<Key, Value> setValues(List<Value> values) {
		mValues = values;
		return this;
	}
}
