package tanchuev.whattodo.model;

import tanchuev.whattodo.model.base.Model;

/**
 * @author tanchuev
 */

public class User extends Model<User> {
	private String mFirstName;
	private String mLastName;
	private String mPatronymic;
	private String mEmail;
	private String mLogin;
	private Role.RoleEnum mRoleEnum;

	public String getFirstName() {
		return mFirstName;
	}

	public User setFirstName(String firstName) {
		mFirstName = firstName;
		return this;
	}

	public String getLastName() {
		return mLastName;
	}

	public User setLastName(String lastName) {
		mLastName = lastName;
		return this;
	}

	public String getPatronymic() {
		return mPatronymic;
	}

	public User setPatronymic(String patronymic) {
		mPatronymic = patronymic;
		return this;
	}

	public String getEmail() {
		return mEmail;
	}

	public User setEmail(String email) {
		mEmail = email;
		return this;
	}

	public String getLogin() {
		return mLogin;
	}

	public User setLogin(String login) {
		mLogin = login;
		return this;
	}

	public Role.RoleEnum getRoleEnum() {
		return mRoleEnum;
	}

	public User setRoleEnum(Role.RoleEnum roleEnum) {
		mRoleEnum = roleEnum;
		return this;
	}
}
