package tanchuev.whattodo.model;


import tanchuev.whattodo.model.base.Model;

/**
 * @author tanchuev
 */
public class Content extends Model<Content> {

	private String mContentUrl;
	private ContentType mContentType;

	public String getContentUrl() {
		return mContentUrl;
	}

	public Content setContentUrl(String contentUrl) {
		mContentUrl = contentUrl;
		return this;
	}

	public ContentType getContentType() {
		return mContentType;
	}

	public Content setContentType(ContentType contentType) {
		mContentType = contentType;
		return this;
	}

	public enum ContentType {
		Photo("1"),
		Gif("2"),
		Audio("3"),
		Video("4");

		private String mId;

		ContentType(String id) {
			mId = id;
		}

		public String getId() {
			return mId;
		}
	}
}
