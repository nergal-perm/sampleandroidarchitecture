package tanchuev.whattodo.model;


import java.util.ArrayList;
import java.util.List;

import tanchuev.whattodo.model.base.ModelName;

/**
 * @author tanchuev
 */
public class Employment extends ModelName<Employment> {
	private int mMinPersonCount;
	private int mMaxPersonCount;
	private int mMinCost;
	private int mMaxCost;
	private String mFullDescription;
	private User mUserAuthor;
	private Content mContent;
	private List<Category> mCategories = new ArrayList<>();
	private List<Location> mLocations = new ArrayList<>();
	private long mRating;
	private boolean isLikedByUser;

	public int getMinPersonCount() {
		return mMinPersonCount;
	}

	public Employment setMinPersonCount(int minPersonCount) {
		mMinPersonCount = minPersonCount;
		return this;
	}

	public int getMaxPersonCount() {
		return mMaxPersonCount;
	}

	public Employment setMaxPersonCount(int maxPersonCount) {
		mMaxPersonCount = maxPersonCount;
		return this;
	}

	public int getMinCost() {
		return mMinCost;
	}

	public Employment setMinCost(int minCost) {
		mMinCost = minCost;
		return this;
	}

	public int getMaxCost() {
		return mMaxCost;
	}

	public Employment setMaxCost(int maxCost) {
		mMaxCost = maxCost;
		return this;
	}

	public String getFullDescription() {
		return mFullDescription;
	}

	public Employment setFullDescription(String fullDescription) {
		mFullDescription = fullDescription;
		return this;
	}

	public User getUserAuthor() {
		return mUserAuthor;
	}

	public Employment setUserAuthor(User userAuthor) {
		mUserAuthor = userAuthor;
		return this;
	}

	public Content getContent() {
		return mContent;
	}

	public Employment setContent(Content content) {
		mContent = content;
		return this;
	}

	public List<Category> getCategories() {
		return mCategories;
	}

	public Employment setCategories(List<Category> categories) {
		mCategories = categories;
		return this;
	}

	public List<Location> getLocations() {
		return mLocations;
	}

	public Employment setLocations(List<Location> locations) {
		mLocations = locations;
		return this;
	}

	public long getRating() {
		return mRating;
	}

	public Employment setRating(long rating) {
		mRating = rating;
		return this;
	}

	public boolean isLikedByUser() {
		return isLikedByUser;
	}

	public Employment setLikedByUser(boolean likedByUser) {
		isLikedByUser = likedByUser;
		return this;
	}
}
