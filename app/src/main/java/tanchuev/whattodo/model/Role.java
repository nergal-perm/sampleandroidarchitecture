package tanchuev.whattodo.model;

import tanchuev.whattodo.model.base.Model;

/**
 * @author tanchuev
 */

public class Role extends Model<Role> {
	public enum RoleEnum {
		Admin("1"),
		User("2");

		private String id;

		RoleEnum(String id) {
			this.id = id;
		}

		private RoleEnum getById(String id) {
			RoleEnum result = null;
			if (id.equals(Admin.id)) {
				result = Admin;
			} else if (id.equals(User.id)) {
				result = User;
			}
			return result;
		}
	}
}
