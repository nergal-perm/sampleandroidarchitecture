package tanchuev.whattodo.model.relationships;

import javax.inject.Inject;

import tanchuev.whattodo.model.Category;
import tanchuev.whattodo.model.Employment;
import tanchuev.whattodo.model.base.Model;

/**
 * @author tanchuev
 */

public class EmploymentLocation extends Model<EmploymentLocation> {
	private Employment mEmployment;
	private Category mCategory;

	@Inject
	public EmploymentLocation() {
	}

	public Employment getEmployment() {
		return mEmployment;
	}

	public EmploymentLocation setEmployment(Employment employment) {
		mEmployment = employment;
		return this;
	}

	public Category getCategory() {
		return mCategory;
	}

	public EmploymentLocation setCategory(Category category) {
		mCategory = category;
		return this;
	}
}
