package tanchuev.whattodo.model.relationships;

import javax.inject.Inject;

import tanchuev.whattodo.model.Category;
import tanchuev.whattodo.model.Employment;
import tanchuev.whattodo.model.base.Model;

/**
 * @author tanchuev
 */

public class EmploymentCategory extends Model<EmploymentCategory> {
	private Employment mEmployment;
	private Category mCategory;

	@Inject
	public EmploymentCategory() {
	}

	public Employment getEmployment() {
		return mEmployment;
	}

	public EmploymentCategory setEmployment(Employment employment) {
		mEmployment = employment;
		return this;
	}

	public Category getCategory() {
		return mCategory;
	}

	public EmploymentCategory setCategory(Category category) {
		mCategory = category;
		return this;
	}
}
