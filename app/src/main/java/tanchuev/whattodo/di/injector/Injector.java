package tanchuev.whattodo.di.injector;

/**
 * @author tanchuev
 */

public interface Injector<Component> {

	Component getComponent();

	Component initComponent();

	void initInjections();
}
