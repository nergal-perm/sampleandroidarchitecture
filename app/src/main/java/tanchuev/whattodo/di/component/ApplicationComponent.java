/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tanchuev.whattodo.di.component;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;
import tanchuev.whattodo.AndroidApplication;
import tanchuev.whattodo.data.DataInterface;
import tanchuev.whattodo.data.provider.Provider;
import tanchuev.whattodo.data.repository.CategoryRepository;
import tanchuev.whattodo.data.repository.EmploymentRepository;
import tanchuev.whattodo.data.repository.LocationRepository;
import tanchuev.whattodo.data.repository.UserRepository;
import tanchuev.whattodo.rx.executor.PostExecutionThread;
import tanchuev.whattodo.rx.executor.ThreadExecutor;
import tanchuev.whattodo.di.modules.ApplicationModule;
import tanchuev.whattodo.di.modules.base.GsonModule;
import tanchuev.whattodo.di.modules.base.ProviderModule;
import tanchuev.whattodo.di.modules.base.RepositoryModule;
import tanchuev.whattodo.di.modules.base.RetrofitModule;
import tanchuev.whattodo.model.Category;
import tanchuev.whattodo.model.Employment;
import tanchuev.whattodo.model.Location;


/**
 * A component whose lifetime is the life of the application.
 */
@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = {
		ApplicationModule.class,
		GsonModule.class,
		RetrofitModule.class,
		RepositoryModule.class,
		ProviderModule.class})
public interface ApplicationComponent {

	void inject(AndroidApplication app);

	SharedPreferences getSharedPreferences();

	ThreadExecutor getThreadExecutor();

	PostExecutionThread getPostExecutionThread();

	Gson getGson();

	Retrofit getRetrofit();

	@Named(DataInterface.MOCK_LABEL) EmploymentRepository getEmploymentMockRepository();

	@Named(DataInterface.SERVER_LABEL) EmploymentRepository getEmploymentServerRepository();

	UserRepository getUserRepository();

	CategoryRepository getCategoryRepository();

	LocationRepository getLocationRepository();

	Provider<Employment> getEmployment();

	Provider<Class> getClassProvider();

	Provider<List<Category>> getCategoriesProvider();

	Provider<List<Location>> getLocationsProvider();
}