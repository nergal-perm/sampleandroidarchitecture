package tanchuev.whattodo.di.component;

import dagger.Component;
import tanchuev.whattodo.di.modules.AuthModule;
import tanchuev.whattodo.di.scope.PerActivity;
import tanchuev.whattodo.ui.activity.AuthActivity;
import tanchuev.whattodo.ui.fragment.AuthFragment;
import tanchuev.whattodo.ui.fragment.RegistrationFragment;


/**
 * @author tanchuev
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {AuthModule.class})
public interface AuthComponent {

	void injectInto(AuthActivity injector);

	void injectInto(AuthFragment injector);

	void injectInto(RegistrationFragment injector);
}