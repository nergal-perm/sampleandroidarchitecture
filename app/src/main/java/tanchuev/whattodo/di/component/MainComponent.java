package tanchuev.whattodo.di.component;

import dagger.Component;
import tanchuev.whattodo.di.modules.MainModule;
import tanchuev.whattodo.di.scope.PerActivity;
import tanchuev.whattodo.ui.activity.MainActivity;
import tanchuev.whattodo.ui.dialog.CategoryListDialogFragment;
import tanchuev.whattodo.ui.dialog.LocationListDialogFragment;
import tanchuev.whattodo.ui.fragment.EmploymentFragment;
import tanchuev.whattodo.ui.fragment.SuggestEmploymentFragment;


/**
 * @author tanchuev
 */
//указываем только MainModule, т.к. остальные зависимости подтянутся через ActivityComponent(в том числе и ApplicationComponent)
//extends от компонента использовать можно, вместо SubComponent, никаких косяков в работе таким способом замечено не было
// это позволяет получать зависимости через методы компонента
//через dependencies = Component.class можно было бы получить все зависимости,
// которые предоставляет ApplicationComponent, но минус такого подхода, что нельзя получать зависимости из компонента родителя
// модули так же нужны чтобы подтягивать необходимые зависимости,
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {MainModule.class})
public interface MainComponent {

	void injectInto(MainActivity injector);

	void injectInto(EmploymentFragment injector);

	void injectInto(CategoryListDialogFragment injector);

	void injectInto(LocationListDialogFragment injector);

	void injectInto(SuggestEmploymentFragment injector);
}