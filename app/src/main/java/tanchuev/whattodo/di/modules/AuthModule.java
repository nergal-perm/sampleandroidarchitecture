package tanchuev.whattodo.di.modules;

import dagger.Binds;
import dagger.Module;
import tanchuev.whattodo.data.service.UserService;
import tanchuev.whattodo.data.service.impl.UserServiceImpl;
import tanchuev.whattodo.ui.presenter.AuthPresenter;
import tanchuev.whattodo.ui.presenter.RegistrationPresenter;
import tanchuev.whattodo.ui.presenter.base.Presenter;
import tanchuev.whattodo.ui.view.AuthView;
import tanchuev.whattodo.ui.view.RegistrationView;

@Module(includes = ActivityModule.class)
public abstract class AuthModule {

	@Binds
	abstract UserService provideUserService(UserServiceImpl userService);

	@Binds
	abstract Presenter<AuthView> provideAuthPresenter(AuthPresenter authPresenter);

	@Binds
	abstract Presenter<RegistrationView> provideRegistrationPresenter(RegistrationPresenter registrationPresenter);

}
