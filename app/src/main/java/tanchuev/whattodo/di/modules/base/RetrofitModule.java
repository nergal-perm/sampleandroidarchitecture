package tanchuev.whattodo.di.modules.base;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import tanchuev.whattodo.BuildConfig;

/**
 * @author tanchuev
 */
@Module
public class RetrofitModule {

	@Provides
	@Singleton
	Retrofit provideRetrofit(Gson gson) {
		return new Retrofit.Builder()
				.baseUrl(BuildConfig.SERVER_URL)
				// need for interceptors
				//.client(OkHttpProvider okHttpProvider)
				.addConverterFactory(GsonConverterFactory.create(gson))
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.build();
	}
}
