package tanchuev.whattodo.di.modules.base;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import tanchuev.whattodo.data.DataInterface;
import tanchuev.whattodo.data.repository.CategoryRepository;
import tanchuev.whattodo.data.repository.EmploymentRepository;
import tanchuev.whattodo.data.repository.LocationRepository;
import tanchuev.whattodo.data.repository.UserRepository;
import tanchuev.whattodo.data.repository.mock.CategoryMockRepository;
import tanchuev.whattodo.data.repository.mock.EmploymentMockRepository;
import tanchuev.whattodo.data.repository.mock.LocationMockRepository;
import tanchuev.whattodo.data.repository.mock.UserMockRepository;
import tanchuev.whattodo.data.repository.server.EmploymentServerRepository;

@Module
public abstract class RepositoryModule {

	@Binds
	@Named(DataInterface.MOCK_LABEL)
	abstract EmploymentRepository provideEmploymentMockRepository(EmploymentMockRepository repository);

	@Binds
	@Named(DataInterface.SERVER_LABEL)
	abstract EmploymentRepository provideEmploymentServerRepository(EmploymentServerRepository repository);

	@Binds
	abstract UserRepository provideUserMockRepository(UserMockRepository repository);

	@Binds
	abstract CategoryRepository provideCategoryMockRepository(CategoryMockRepository repository);

	@Binds
	abstract LocationRepository provideLocationMockRepository(LocationMockRepository repository);

}
