/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tanchuev.whattodo.di.modules;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import tanchuev.whattodo.AndroidApplication;
import tanchuev.whattodo.rx.executor.JobExecutor;
import tanchuev.whattodo.rx.executor.PostExecutionThread;
import tanchuev.whattodo.rx.executor.ThreadExecutor;
import tanchuev.whattodo.rx.executor.UIThread;

/**
 * Dagger module that provides objects which will live during the application lifecycle.
 */
@Module
public class ApplicationModule {
	private static final String SHARED_PREFERENCES_KEY = "tanchuev.whattodo";
	private final AndroidApplication mApplication;
	private final SharedPreferences mSharedPreferences;

	public ApplicationModule(AndroidApplication application) {
		mApplication = application;
		mSharedPreferences = mApplication.getSharedPreferences(SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
	}

	@Provides
	@Singleton
	Context provideApplicationContext() {
		return mApplication;
	}

	@Provides
	@Singleton
	SharedPreferences provideSharedPreferences() {
		return mSharedPreferences;
	}

	@Provides
	@Singleton
	ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
		return jobExecutor;
	}

	@Provides
	@Singleton
	PostExecutionThread providePostExecutionThread(UIThread uiThread) {
		return uiThread;
	}
}
