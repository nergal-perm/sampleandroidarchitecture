package tanchuev.whattodo.di.modules.base;

import java.util.List;

import dagger.Binds;
import dagger.Module;
import tanchuev.whattodo.data.provider.Provider;
import tanchuev.whattodo.data.provider.ProviderImpl;
import tanchuev.whattodo.model.Category;
import tanchuev.whattodo.model.Employment;
import tanchuev.whattodo.model.Location;

/**
 * @author tanchuev
 */
@Module
public abstract class ProviderModule {
	@Binds
	abstract Provider<Employment> provideEmployment(ProviderImpl<Employment> provider);

	@Binds
	abstract Provider<Class> provideClass(ProviderImpl<Class> provider);

	@Binds
	abstract Provider<List<Category>> provideCategories(ProviderImpl<List<Category>> provider);

	@Binds
	abstract Provider<List<Location>> provideLocations(ProviderImpl<List<Location>> provider);
}
