package tanchuev.whattodo.di.modules;

import dagger.Binds;
import dagger.Module;
import tanchuev.whattodo.data.service.CategoryService;
import tanchuev.whattodo.data.service.EmploymentService;
import tanchuev.whattodo.data.service.LocationService;
import tanchuev.whattodo.data.service.UserService;
import tanchuev.whattodo.data.service.impl.CategoryServiceImpl;
import tanchuev.whattodo.data.service.impl.EmploymentServiceImpl;
import tanchuev.whattodo.data.service.impl.LocationServiceImpl;
import tanchuev.whattodo.data.service.impl.UserServiceImpl;
import tanchuev.whattodo.ui.presenter.EmploymentPresenter;
import tanchuev.whattodo.ui.presenter.SuggestEmploymentPresenter;
import tanchuev.whattodo.ui.presenter.base.Presenter;
import tanchuev.whattodo.ui.view.EmploymentView;
import tanchuev.whattodo.ui.view.SuggestEmploymentView;

@Module(includes = ActivityModule.class)
public abstract class MainModule {

	@Binds
	abstract UserService provideUserService(UserServiceImpl userMockRepository);

	@Binds
	abstract EmploymentService provideEmploymentService(EmploymentServiceImpl employmentMockRepository);

	@Binds
	abstract CategoryService provideCategoryService(CategoryServiceImpl categoryMockRepository);

	@Binds
	abstract LocationService provideLocationService(LocationServiceImpl locationMockRepository);

	@Binds
	abstract Presenter<EmploymentView> provideEmploymentPresenter(EmploymentPresenter employmentPresenter);

	@Binds
	abstract Presenter<SuggestEmploymentView> provideSuggestEmploymentPresenter(SuggestEmploymentPresenter suggestEmploymentPresenter);
}
