package tanchuev.whattodo.di.modules.base;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author tanchuev
 */
@Module
public class GsonModule {
	private Gson mGson;

	@Provides
	@Singleton
	Gson provideGson(/*NewsDeserializer newsDeserializer*/) {
		if (mGson == null) {
			mGson = new GsonBuilder()
					.excludeFieldsWithoutExposeAnnotation()
					// can not serialize nulls because it will affect optional fields in requests which should not be serialized
					// and we use null to not serialize them
					//.serializeNulls()
					// type adapters
					//.registerTypeAdapter(NewsItem.class, new NewsDeserializer())
					//.setDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSS'Z'")
					.create();
		}
		return mGson;
	}

}
