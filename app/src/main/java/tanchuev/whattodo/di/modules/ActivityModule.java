package tanchuev.whattodo.di.modules;

import dagger.Module;
import dagger.Provides;
import tanchuev.whattodo.di.scope.PerActivity;
import tanchuev.whattodo.navigation.Navigator;
import tanchuev.whattodo.navigation.NavigatorImpl;
import tanchuev.whattodo.ui.activity.base.InjectorActivity;

@Module
public class ActivityModule {
	private InjectorActivity mActivity;

	public ActivityModule(InjectorActivity activity) {
		mActivity = activity;
	}

	@Provides
	@PerActivity
	Navigator provideNavigator(NavigatorImpl navigator) {
		return navigator;
	}

	@Provides
	@PerActivity
	InjectorActivity provideActivity() {
		return mActivity;
	}

}
