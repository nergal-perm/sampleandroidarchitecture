package tanchuev.whattodo.utils;

import java.util.concurrent.atomic.AtomicInteger;

public final class SequenceUtil {
	private static final AtomicInteger seed = new AtomicInteger();

	public static int getNextInt() {
		return seed.incrementAndGet();
	}
}
