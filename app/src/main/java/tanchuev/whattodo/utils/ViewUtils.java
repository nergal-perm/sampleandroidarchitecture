package tanchuev.whattodo.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

import tanchuev.whattodo.R;
import tanchuev.whattodo.model.base.ModelRelationship;

public final class ViewUtils {

	private ViewUtils() {
	}

	public static float pxToDp(float px) {
		float densityDpi = Resources.getSystem().getDisplayMetrics().densityDpi;
		return px / (densityDpi / 160f);
	}

	public static int dpToPx(float dp) {
		float density = Resources.getSystem().getDisplayMetrics().density;
		return Math.round(dp * density);
	}

	public static int spToPx(float sp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, Resources.getSystem().getDisplayMetrics());
	}

	public static int getDisplayWidth() {
		return Resources.getSystem().getDisplayMetrics().widthPixels;
	}

	public static int getDisplayHeight() {
		return Resources.getSystem().getDisplayMetrics().heightPixels;
	}

	public static void hideKeyboard(Activity activity) {
		InputMethodManager imm =
				(InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
	}

	public static void setVisibility(@Visibility int visibility, @Nullable View... views) {
		if (views == null || views.length == 0) {
			return;
		}
		for (View v : views) {
			if (v != null && v.getVisibility() != visibility) v.setVisibility(visibility);
		}
	}

	public static void removeOnGlobalLayoutListener(View v, ViewTreeObserver.OnGlobalLayoutListener listener) {
		v.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
	}

	public void showDialogMessage(Context context, String message, String title) {
		new AlertDialog.Builder(context)
				.setMessage(message)
				.setTitle(title)
				.setPositiveButton(R.string.OK,
						(dialog, which) -> dialog.dismiss())
				.show();
	}

	public static void showToastMessage(Context context, @StringRes int message, int duration) {
		showToastMessage(context, context.getString(message), duration);
	}

	public static void showToastMessage(Context context, String message, int duration) {
		if (Utils.isNullOrEmpty(message)) {
			return;
		}
		Toast.makeText(context.getApplicationContext(), message, duration).show();
	}

	// region FOR CURRENT APP

	public static void setValuesToView(List<? extends ModelRelationship> modelRelationships,
									   TextView tv,
									   String separator) {
		for (int i = 0; i < modelRelationships.size(); i++) {
			tv.append(modelRelationships.get(i).getName());
			if (i != modelRelationships.size() - 1) {
				tv.append(separator);
			}
		}
	}

	// endregion FOR CURRENT APP

	@IntDef({View.VISIBLE, View.INVISIBLE, View.GONE})
	@Retention(RetentionPolicy.SOURCE)
	public @interface Visibility {
	}

}