package tanchuev.whattodo.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

public final class Utils {
	private static final DateFormat dd_MM_yyyy_HH_mm = new SimpleDateFormat("dd.MM.yyyy HH:mm");

	public static DateFormat getDateFormatWithTime() {
		return dd_MM_yyyy_HH_mm;
	}

	public static String getStringDateWithTime() {
		Date cal = Calendar.getInstance().getTime();
		return getDateFormatWithTime().format(cal);
	}

	public static boolean isNullOrEmpty(Object o) {
		if (o == null) {
			return true;
		}
		if (o instanceof CharSequence) {
			return ((CharSequence) o).length() == 0;
		}
		if (o instanceof Collection) {
			return ((Collection) o).isEmpty();
		}
		if (o instanceof Map) {
			return ((Map) o).isEmpty();
		}
		return true;
	}
}
