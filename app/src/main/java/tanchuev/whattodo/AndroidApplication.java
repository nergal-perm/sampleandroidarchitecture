package tanchuev.whattodo;

import android.app.Application;

import tanchuev.whattodo.di.component.ApplicationComponent;
import tanchuev.whattodo.di.component.DaggerApplicationComponent;
import tanchuev.whattodo.di.modules.ApplicationModule;
import timber.log.Timber;

public class AndroidApplication extends Application {

	private ApplicationComponent applicationComponent;

	@Override
	public void onCreate() {
		super.onCreate();
		if (BuildConfig.DEBUG) {
			Timber.plant(new Timber.DebugTree());
		}
		this.initializeInjector();
	}

	private void initializeInjector() {
		this.applicationComponent = DaggerApplicationComponent.builder()
				.applicationModule(new ApplicationModule(this))
				.build();
	}

	public ApplicationComponent getApplicationComponent() {
		return this.applicationComponent;
	}
}
