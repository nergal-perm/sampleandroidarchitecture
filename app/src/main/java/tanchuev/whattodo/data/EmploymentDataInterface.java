package tanchuev.whattodo.data;

import android.support.v4.util.Pair;

import rx.Observable;
import tanchuev.whattodo.model.Employment;
import tanchuev.whattodo.model.Filter;

public interface EmploymentDataInterface extends DataInterface<Employment> {

	Observable<Employment> getRandom();

	Observable<Employment> getFiltered(Filter filter);

	Observable<Pair<Boolean, Long>> like(String id);
}
