package tanchuev.whattodo.data;

import rx.Observable;
import tanchuev.whattodo.model.base.Model;

/**
 * @author tanchuev
 */

public interface DataInterface<T extends Model> {
	String MOCK_LABEL = "Mock";
	String LOCAL_LABEL = "Local";
	String SERVER_LABEL = "Server";

	Observable<T> getAll();

	Observable<T> get(String id);

	Observable<Boolean> create(T t);

	Observable<Boolean> update(T t);

	Observable<Boolean> delete(String id);

	Observable<Boolean> deleteAll();
}
