package tanchuev.whattodo.data.repository.server;

import android.support.v4.util.Pair;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Retrofit;
import rx.Observable;
import tanchuev.whattodo.data.api.EmploymentApi;
import tanchuev.whattodo.data.repository.EmploymentRepository;
import tanchuev.whattodo.data.repository.base.ServerRepository;
import tanchuev.whattodo.model.Employment;
import tanchuev.whattodo.model.Filter;

@Singleton
public class EmploymentServerRepository
		extends ServerRepository<Employment, EmploymentApi>
		implements EmploymentRepository {

	@Inject
	public EmploymentServerRepository(Retrofit retrofit) {
		super(retrofit);
	}

	@Override
	public Observable<Employment> getRandom() {
		return null;
	}

	@Override
	public Observable<Employment> getFiltered(Filter filter) {
		return null;
	}

	@Override
	public Observable<Pair<Boolean, Long>> like(String id) {
		return null;
	}

	@Override
	protected Class getApiClass() {
		return EmploymentApi.class;
	}
}
