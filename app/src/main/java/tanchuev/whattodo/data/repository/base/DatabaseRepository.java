package tanchuev.whattodo.data.repository.base;

import rx.Observable;
import tanchuev.whattodo.model.base.Model;

/**
 * @author tanchuev
 */
public abstract class DatabaseRepository<T extends Model> implements Repository<T> {
	@Override
	public Observable<T> getAll() {
		return Observable.empty();
	}

	@Override
	public Observable<T> get(String id) {
		return Observable.empty();
	}

	@Override
	public Observable<Boolean> create(T t) {
		return Observable.empty();
	}

	@Override
	public Observable<Boolean> update(T t) {
		return Observable.empty();
	}

	@Override
	public Observable<Boolean> delete(String id) {
		return Observable.empty();
	}

	@Override
	public Observable<Boolean> deleteAll() {
		return Observable.empty();
	}

    /*private Class<T> tClass;
	private Property<String> mProperty;

    public DatabaseRepository(Property<String> property, Class<T> tClass) {
        this.tClass = tClass;
        mProperty = property;
    }

    @Override
    public Observable<T> getAll() {
        return Observable.just(true)
                .flatMap(new Func1<Boolean, Observable<T>>() {
                    @Override
                    public Observable<T> call(Boolean aBoolean) {
                        return Observable.from(
                                new Select()
                                .from(tClass)
                                .queryList());
                    }
                });
    }

    @Override
    public Observable<T> get(final String id) {
        return Observable.just(true)
                .map(new Func1<Boolean, T>() {
                    @Override
                    public T call(Boolean aBoolean) {
                        return new Select()
                                .from(tClass)
                                .where(mProperty.eq(id))
                                .querySingle();
                    }
                });
    }

    @Override
    public Observable<List<T>> get(final List<String> ids) {
        return Observable.just(true)
                .map(new Func1<Boolean, List<T>>() {
                    @Override
                    public List<T> call(Boolean aBoolean) {
                        return new Select()
                                .from(tClass)
                                .where(mProperty.in(ids))
                                .queryList();
                    }
                });
    }

    @Override
    public Observable<Boolean> createOrUpdate(final T t) {

        *//*if (new Select()
                .from(tClass)
                .where(mProperty.eq(t.getId()))
                .querySingle() == null) {
            t.save();
        } else {
            t.createOrUpdate();
        }*//*

        return Observable.just(true)
                .map(new Func1<Boolean, Boolean>() {
                    @Override
                    public Boolean call(Boolean aBoolean) {
                        if (t.exists()) {
                            t.update();
                        } else {
                            t.save();
                        }
                        return true;
                    }
                });
    }

    @Override
    public Observable<Boolean> delete(final T t) {
        return Observable.just(true)
                .map(new Func1<Boolean, Boolean>() {
                    @Override
                    public Boolean call(Boolean aBoolean) {
                        t.delete();
                        return true;
                    }
                });
    }

    @Override
    public Observable<Boolean> deleteAll() {
        return Observable.just(true)
                .map(new Func1<Boolean, Boolean>() {
                    @Override
                    public Boolean call(Boolean aBoolean) {
                        Delete.table(tClass);
                        return true;
                    }
                });
    }*/


}
