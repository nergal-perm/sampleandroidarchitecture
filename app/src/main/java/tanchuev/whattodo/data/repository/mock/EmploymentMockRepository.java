package tanchuev.whattodo.data.repository.mock;

import android.support.v4.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import tanchuev.whattodo.data.repository.EmploymentRepository;
import tanchuev.whattodo.data.repository.base.MockRepository;
import tanchuev.whattodo.model.Category;
import tanchuev.whattodo.model.Content;
import tanchuev.whattodo.model.Employment;
import tanchuev.whattodo.model.Filter;
import tanchuev.whattodo.model.Location;
import tanchuev.whattodo.utils.Utils;

import static tanchuev.whattodo.data.repository.mock.CategoryMockRepository.categories;

@Singleton
public class EmploymentMockRepository
		extends MockRepository<Employment>
		implements EmploymentRepository {
	public static List<Employment> sEmployments = new ArrayList<>();
	private int mLastPos;

	@Inject
	public EmploymentMockRepository() {
		super(sEmployments);
		init();
	}

	public Observable<Employment> getRandom() {
		if (mLastPos == sEmployments.size() - 1) {
			mLastPos = 0;
		}
		return Observable.just(sEmployments.get(mLastPos++))
				.delay(2, TimeUnit.SECONDS);
	}

	@Override
	public Observable<Employment> getFiltered(Filter filter) {
		return getAll()
				.firstOrDefault(null, employment -> {

					if (filter.isEmpty()) {
						return false;
					}

					boolean result = false;

					if (!Utils.isNullOrEmpty(filter.getCategories())) {
						for (Category category : employment.getCategories()) {
							for (Category categoryFilter : filter.getCategories()) {
								if (category.equals(categoryFilter)) {
									result = true;
								}
							}
						}
					}

					if (!Utils.isNullOrEmpty(filter.getLocations())) {
						for (Location location : employment.getLocations()) {
							for (Location locationFilter : filter.getLocations()) {
								if (location.equals(locationFilter)) {
									result = true;
								}
							}
						}
					}

					if ((filter.getMinCost() != 0 || filter.getMaxCost() != 0)
							&& (filter.getMinCost() <= employment.getMinCost()
							|| filter.getMaxCost() >= employment.getMaxCost())) {
						result = true;
					}

					if ((filter.getMinPersonCount() != 0 || filter.getMaxPersonCount() != 0)
							&& (filter.getMinPersonCount() <= employment.getMinPersonCount()
							|| filter.getMaxPersonCount() >= employment.getMaxPersonCount())) {
						result = true;
					}
					return result;
				});
	}

	public Observable<Pair<Boolean, Long>> like(String id) {
		return get(id)
				.map(employment -> {
					employment.setLikedByUser(!employment.isLikedByUser());
					if (employment.isLikedByUser()) {
						employment.setRating(employment.getRating() + 1);
					} else {
						employment.setRating(employment.getRating() - 1);
					}
					return new Pair<>(employment.isLikedByUser(), employment.getRating());
				})
				.delay(1, TimeUnit.SECONDS);
	}

	public static void init() {
		CategoryMockRepository.init();
		LocationMockRepository.init();
		if (sEmployments.size() == 0) {
			sEmployments.add(new Employment()
					.setCategories(generateCategoryList())
					.setLocations(generateLocationList())
					.setMinCost(0)
					.setMaxCost(0)
					.setMinPersonCount(1)
					.setMaxPersonCount(0)
					.setRating(0)
					.setLikedByUser(false)
					.setName("Смотреть ковер")
					.setFullDescription("Что может быть лучше чем смотреть ковер?\nТолько смотреть еще один ковер!")
					.setContent(new Content()
							.setContentType(Content.ContentType.Photo)
							.setContentUrl("https://pp.vk.me/c305515/u27365370/155452578/x_af7778da.jpg")));
			sEmployments.add(new Employment()
					.setCategories(generateCategoryList())
					.setLocations(generateLocationList())
					.setMinCost(10000)
					.setMaxCost(10000000)
					.setMinPersonCount(1)
					.setMaxPersonCount(0)
					.setRating(11)
					.setName("Поездка на море")
					.setFullDescription("Съездите на море на эти выходные!")
					.setLikedByUser(false)
					.setContent(new Content()
							.setContentType(Content.ContentType.Photo)
							.setContentUrl("http://oboi-dlja-stola.ru/file/11616/760x0/16:9/%D0%9C%D0%BE%D1%80%D0%B5-%D0%B8-%D0%BD%D0%B5%D0%B1%D0%BE.jpg")));
			sEmployments.add(new Employment()
					.setCategories(generateCategoryList())
					.setLocations(generateLocationList())
					.setMinCost(0)
					.setMaxCost(50000)
					.setMinPersonCount(0)
					.setMaxPersonCount(0)
					.setRating(25)
					.setLikedByUser(true)
					.setName("Заведите питомца")
					.setFullDescription("Купите себе питомца, жизнь станет веселее!")
					.setContent(new Content()
							.setContentType(Content.ContentType.Photo)
							.setContentUrl("http://kartiny.ucoz.ru/_ph/632/2/184319378.jpg")));
			sEmployments.add(new Employment()
					.setCategories(generateCategoryList())
					.setLocations(generateLocationList())
					.setMinCost(1000)
					.setMaxCost(3000)
					.setMinPersonCount(1)
					.setMaxPersonCount(0)
					.setRating(2)
					.setLikedByUser(false)
					.setName("Сыграйте в монополию")
					.setFullDescription("Очень интересная игра, вот правила: \n" +
							"http://www.tvoihod.ru/boardgames/monopoly/monopoly-rules/")
					.setContent(new Content()
							.setContentType(Content.ContentType.Photo)
							.setContentUrl("http://s1.cdnproductmain.mosigra.ru/525/945/IMG_8689_800x500.jpg")));
			sEmployments.add(new Employment()
					.setCategories(generateCategoryList())
					.setLocations(generateLocationList())
					.setMinCost(Integer.MAX_VALUE)
					.setMaxCost(Integer.MAX_VALUE)
					.setMinPersonCount(1)
					.setMaxPersonCount(0)
					.setRating(151)
					.setLikedByUser(false)
					.setName("Слетайте на луну!")
					.setFullDescription("Ведь все это делают!")
					.setContent(new Content()
							.setContentType(Content.ContentType.Photo)
							.setContentUrl("http://www.e-news.su/uploads/posts/2015-06/1434481027_153744.jpg")));
			sEmployments.add(new Employment()
					.setCategories(generateCategoryList())
					.setLocations(generateLocationList())
					.setMinCost(0)
					.setMaxCost(0)
					.setMinPersonCount(1)
					.setMaxPersonCount(0)
					.setRating(69)
					.setLikedByUser(false)
					.setName("Признайтесь в любви!")
					.setFullDescription("Любовь - самое лучшее чувство на свете!")
					.setContent(new Content()
							.setContentType(Content.ContentType.Photo)
							.setContentUrl("http://www.yourfreedom.ru/custom-content/uploads/118783_or.jpg")));
		}
	}

	private static List<Category> generateCategoryList() {
		List<Category> result = new ArrayList<>();
		for (int i = 0; i < categories.size(); i++) {
			if (new Random().nextBoolean())
				result.add(categories.get(i));
		}
		return result;
	}

	private static List<Location> generateLocationList() {
		List<Location> result = new ArrayList<>();
		for (int i = 0; i < LocationMockRepository.locations.size(); i++) {
			if (new Random().nextBoolean())
				result.add(LocationMockRepository.locations.get(i));
		}
		return result;
	}
}
