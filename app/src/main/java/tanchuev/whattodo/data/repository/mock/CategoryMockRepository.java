package tanchuev.whattodo.data.repository.mock;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import tanchuev.whattodo.data.repository.CategoryRepository;
import tanchuev.whattodo.data.repository.base.MockRepository;
import tanchuev.whattodo.model.Category;

@Singleton
public class CategoryMockRepository
		extends MockRepository<Category>
		implements CategoryRepository {
	public static List<Category> categories = new ArrayList<>();

	@Inject
	public CategoryMockRepository() {
		super(categories);
		init();
	}

	public static void init() {
		if (categories.size() == 0) {
			categories.add(new Category()
					.setName("Спорт"));
			categories.add(new Category()
					.setName("Обучение"));
			categories.add(new Category()
					.setName("Отдых"));
			categories.add(new Category()
					.setName("Полезное"));
			categories.add(new Category()
					.setName("Веселящее"));
			categories.add(new Category()
					.setName("Грустное"));
			categories.add(new Category()
					.setName("Скучное"));
			categories.add(new Category()
					.setName("Сумасшедшее"));
			categories.add(new Category()
					.setName("Смелое"));
		}
	}
}
