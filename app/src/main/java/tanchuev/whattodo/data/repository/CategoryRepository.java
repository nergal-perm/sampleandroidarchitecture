package tanchuev.whattodo.data.repository;

import tanchuev.whattodo.data.repository.base.Repository;
import tanchuev.whattodo.model.Category;

public interface CategoryRepository extends Repository<Category> {
}
