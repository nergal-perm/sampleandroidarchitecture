package tanchuev.whattodo.data.repository;

import tanchuev.whattodo.data.repository.base.Repository;
import tanchuev.whattodo.model.Location;

public interface LocationRepository extends Repository<Location> {
}
