package tanchuev.whattodo.data.repository.base;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import tanchuev.whattodo.model.base.Model;

/**
 * @author tanchuev
 */
public abstract class MockRepository<T extends Model> implements Repository<T> {

	protected List<T> mItems;

	public MockRepository(List<T> items) {
		mItems = items;
	}

	@Override
	public Observable<T> getAll() {
		return Observable.from(mItems)
				.delay(2, TimeUnit.SECONDS);
	}

	@Override
	public Observable<T> get(final String id) {
		return Observable.from(mItems)
				.filter(t -> t.getId().equals(id))
				.delay(2, TimeUnit.SECONDS);
	}

	@Override
	public Observable<Boolean> create(T t) {
		return Observable.just(mItems)
				.map(ts -> {
					if (!ts.contains(t)) {
						mItems.add(t);
						return true;
					}
					return false;
				})
				.delay(2, TimeUnit.SECONDS);
	}

	@Override
	public Observable<Boolean> update(T t) {
		return Observable.just(mItems)
				.map(ts -> {
					for (int i = 0; i < ts.size(); i++) {
						if (ts.get(i).equals(t)) {
							mItems.set(i, t);
							return true;
						}
					}
					return false;
				})
				.delay(2, TimeUnit.SECONDS);
	}

	@Override
	public Observable<Boolean> delete(String id) {
		for (T item : mItems) {
			if (item.getId().equals(id)) {
				mItems.remove(item);
			}
		}
		return Observable.just(true)
				.delay(2, TimeUnit.SECONDS);
	}

	@Override
	public Observable<Boolean> deleteAll() {
		mItems.clear();
		return Observable.just(true)
				.delay(2, TimeUnit.SECONDS);
	}
}
