package tanchuev.whattodo.data.repository.base;

import tanchuev.whattodo.data.DataInterface;
import tanchuev.whattodo.model.base.Model;

/**
 * @author tanchuev
 */

public interface Repository<T extends Model> extends DataInterface<T> {
}
