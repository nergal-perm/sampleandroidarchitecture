package tanchuev.whattodo.data.repository;

import tanchuev.whattodo.data.EmploymentDataInterface;
import tanchuev.whattodo.data.repository.base.Repository;
import tanchuev.whattodo.model.Employment;

public interface EmploymentRepository extends EmploymentDataInterface, Repository<Employment> {
}
