package tanchuev.whattodo.data.repository.mock;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import tanchuev.whattodo.data.repository.UserRepository;
import tanchuev.whattodo.data.repository.base.MockRepository;
import tanchuev.whattodo.model.User;

@Singleton
public class UserMockRepository
		extends MockRepository<User>
		implements UserRepository {
	public static List<User> users = new ArrayList<>();

	@Inject
	public UserMockRepository() {
		super(users);
	}
}
