package tanchuev.whattodo.data.repository.base;

import retrofit2.Retrofit;
import rx.Observable;
import tanchuev.whattodo.model.base.Model;

/**
 * @author tanchuev
 */
public abstract class ServerRepository<T extends Model, TApi> implements Repository<T> {

	protected TApi mApi;

	public ServerRepository(Retrofit retrofit) {
		mApi = retrofit.create(getApiClass());
	}

	protected abstract Class<TApi> getApiClass();

	@Override
	public Observable<T> getAll() {
		return Observable.empty();
	}

	@Override
	public Observable<T> get(String id) {
		return Observable.empty();
	}

	@Override
	public Observable<Boolean> create(T t) {
		return Observable.empty();
	}

	@Override
	public Observable<Boolean> update(T t) {
		return Observable.empty();
	}

	@Override
	public Observable<Boolean> delete(String id) {
		return Observable.empty();
	}

	@Override
	public Observable<Boolean> deleteAll() {
		return Observable.empty();
	}
}
