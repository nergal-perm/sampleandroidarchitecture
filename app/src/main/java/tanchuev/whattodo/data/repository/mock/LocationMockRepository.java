package tanchuev.whattodo.data.repository.mock;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import tanchuev.whattodo.data.repository.LocationRepository;
import tanchuev.whattodo.data.repository.base.MockRepository;
import tanchuev.whattodo.model.Location;

@Singleton
public class LocationMockRepository
		extends MockRepository<Location>
		implements LocationRepository {
	public static List<Location> locations = new ArrayList<>();

	@Inject
	public LocationMockRepository() {
		super(locations);
	}

	public static void init() {
		if (locations.size() == 0) {
			locations.add(new Location()
					.setName("Лес"));
			locations.add(new Location()
					.setName("Река"));
			locations.add(new Location()
					.setName("Город"));
			locations.add(new Location()
					.setName("Поле"));
			locations.add(new Location()
					.setName("Космос"));
			locations.add(new Location()
					.setName("Пещера"));
			locations.add(new Location()
					.setName("Параллельный мир"));
			locations.add(new Location()
					.setName("Планета Татуин"));
			locations.add(new Location()
					.setName("Африка"));
		}
	}

}
