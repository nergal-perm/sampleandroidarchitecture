package tanchuev.whattodo.data.repository;

import tanchuev.whattodo.data.repository.base.Repository;
import tanchuev.whattodo.model.User;

public interface UserRepository extends Repository<User> {
}
