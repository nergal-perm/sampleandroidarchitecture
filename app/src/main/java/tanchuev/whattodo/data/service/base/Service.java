package tanchuev.whattodo.data.service.base;

import tanchuev.whattodo.data.DataInterface;
import tanchuev.whattodo.model.base.Model;

/**
 * @author tanchuev
 */

public interface Service<T extends Model> extends DataInterface<T> {
	//интерфейс, который реализуется для каждой модели данных(entity)
	//отвечает за работу с репозиториями(mock, db, server, preferences?, etc), обработку данных
	//Presenter инжектит необходимые сервисы и работает с ними с помощью RxTaskExecutor
}
