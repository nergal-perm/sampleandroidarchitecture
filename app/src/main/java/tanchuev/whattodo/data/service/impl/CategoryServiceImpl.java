package tanchuev.whattodo.data.service.impl;

import javax.inject.Inject;

import tanchuev.whattodo.data.repository.CategoryRepository;
import tanchuev.whattodo.data.repository.base.Repository;
import tanchuev.whattodo.data.service.CategoryService;
import tanchuev.whattodo.data.service.base.SingleRepositoryService;
import tanchuev.whattodo.di.scope.PerActivity;
import tanchuev.whattodo.model.Category;

/**
 * @author tanchuev
 */

@PerActivity
public class CategoryServiceImpl
		extends SingleRepositoryService<Repository<Category>, Category>
		implements CategoryService {

	@Inject
	public CategoryServiceImpl(CategoryRepository repository) {
		super(repository);
	}

}