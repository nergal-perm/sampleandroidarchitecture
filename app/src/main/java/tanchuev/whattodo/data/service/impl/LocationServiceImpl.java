package tanchuev.whattodo.data.service.impl;

import javax.inject.Inject;

import tanchuev.whattodo.data.repository.LocationRepository;
import tanchuev.whattodo.data.service.LocationService;
import tanchuev.whattodo.data.service.base.SingleRepositoryService;
import tanchuev.whattodo.di.scope.PerActivity;
import tanchuev.whattodo.model.Location;

/**
 * @author tanchuev
 */

@PerActivity
public class LocationServiceImpl
		extends SingleRepositoryService<LocationRepository, Location>
		implements LocationService {

	@Inject
	public LocationServiceImpl(LocationRepository repository) {
		super(repository);
	}

}