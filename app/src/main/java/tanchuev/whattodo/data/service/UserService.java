package tanchuev.whattodo.data.service;

import tanchuev.whattodo.data.service.base.Service;
import tanchuev.whattodo.model.User;

/**
 * @author tanchuev
 */

public interface UserService extends Service<User> {

}