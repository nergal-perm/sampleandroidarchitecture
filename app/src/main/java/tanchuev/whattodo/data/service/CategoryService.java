package tanchuev.whattodo.data.service;

import tanchuev.whattodo.data.service.base.Service;
import tanchuev.whattodo.model.Category;

/**
 * @author tanchuev
 */

public interface CategoryService extends Service<Category> {

}