package tanchuev.whattodo.data.service;

import tanchuev.whattodo.data.EmploymentDataInterface;
import tanchuev.whattodo.data.service.base.Service;
import tanchuev.whattodo.model.Employment;

/**
 * @author tanchuev
 */

public interface EmploymentService extends EmploymentDataInterface, Service<Employment> {

}