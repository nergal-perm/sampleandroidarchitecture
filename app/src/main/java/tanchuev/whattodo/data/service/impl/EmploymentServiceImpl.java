package tanchuev.whattodo.data.service.impl;

import android.support.v4.util.Pair;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import tanchuev.whattodo.data.DataInterface;
import tanchuev.whattodo.data.repository.EmploymentRepository;
import tanchuev.whattodo.data.service.EmploymentService;
import tanchuev.whattodo.data.service.base.SingleRepositoryService;
import tanchuev.whattodo.di.scope.PerActivity;
import tanchuev.whattodo.model.Employment;
import tanchuev.whattodo.model.Filter;

/**
 * @author tanchuev
 */

@PerActivity
public class EmploymentServiceImpl
		extends SingleRepositoryService<EmploymentRepository, Employment>
		implements EmploymentService {

	@Inject
	public EmploymentServiceImpl(@Named(DataInterface.MOCK_LABEL) EmploymentRepository mockRepository) {
		super(mockRepository);
	}

	public Observable<Employment> getRandom() {
		return mRepository.getRandom();
	}

	@Override
	public Observable<Employment> getFiltered(Filter filter) {
		return mRepository.getFiltered(filter);
	}

	public Observable<Pair<Boolean, Long>> like(String id) {
		return mRepository.like(id);
	}
}