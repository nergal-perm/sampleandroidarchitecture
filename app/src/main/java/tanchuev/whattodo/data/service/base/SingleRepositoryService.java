package tanchuev.whattodo.data.service.base;

import rx.Observable;
import tanchuev.whattodo.model.base.Model;
import tanchuev.whattodo.data.repository.base.Repository;

public abstract class SingleRepositoryService<TRepository extends Repository<T>, T extends Model<T>> implements Service<T> {
	// сервис для работы с 1 типом репозитория,
	// если нужно будет 2 или более репозиториев в сервисе,
	// то нужно будет делать базовый сервис, который будет описывать логику работы с ними
	// методы, которым нужна логика, отличная от базовых, должны описываться в конкретных сервисах

	protected TRepository mRepository;

	public SingleRepositoryService(TRepository repository) {
		this.mRepository = repository;
	}

	@Override
	public Observable<T> getAll() {
		return mRepository.getAll();
	}

	@Override
	public Observable<T> get(String id) {
		return mRepository.get(id);
	}

	@Override
	public Observable<Boolean> create(T t) {
		return mRepository.create(t);
	}

	@Override
	public Observable<Boolean> update(T t) {
		return mRepository.update(t);
	}

	@Override
	public Observable<Boolean> delete(String id) {
		return mRepository.delete(id);
	}

	@Override
	public Observable<Boolean> deleteAll() {
		return mRepository.deleteAll();
	}
}
