package tanchuev.whattodo.data.service;

import tanchuev.whattodo.data.service.base.Service;
import tanchuev.whattodo.model.Location;

/**
 * @author tanchuev
 */

public interface LocationService extends Service<Location> {

}