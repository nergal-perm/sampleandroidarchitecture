package tanchuev.whattodo.data.service.impl;

import javax.inject.Inject;

import tanchuev.whattodo.data.repository.UserRepository;
import tanchuev.whattodo.data.service.UserService;
import tanchuev.whattodo.data.service.base.SingleRepositoryService;
import tanchuev.whattodo.di.scope.PerActivity;
import tanchuev.whattodo.model.User;

/**
 * @author tanchuev
 */
@PerActivity
public class UserServiceImpl
		extends SingleRepositoryService<UserRepository, User>
		implements UserService {

	@Inject
	public UserServiceImpl(UserRepository repository) {
		super(repository);
	}

}