package tanchuev.whattodo.data.provider;

/**
 * Created by marat.taychinov
 */

public interface Provider<T> {

	T get();

	void set(T object);
}
