package tanchuev.whattodo.data.provider;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * @author tanchuev
 */
@Singleton
public class ProviderImpl<T> implements Provider<T> {

	private T mItem;

	@Inject
	public ProviderImpl() {
	}

	@Override
	public T get() {
		return mItem;
	}

	@Override
	public void set(T object) {
		mItem = object;
	}
}
