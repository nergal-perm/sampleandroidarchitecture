package tanchuev.whattodo.data.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import tanchuev.whattodo.model.Employment;

/**
 * Created by marat.taychinov
 */

public class TestRequest {

	@SerializedName("test")
	@Expose
	String mString;

	@SerializedName("employment")
	@Expose
	Employment mEmployment;

	@SerializedName("test2")
	@Expose
	String mString2;

	public TestRequest(String string, Employment employment, String string2) {
		mString = string;
		mEmployment = employment;
		mString2 = string2;
	}
}
