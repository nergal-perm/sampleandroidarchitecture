package tanchuev.whattodo.data.api;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;
import tanchuev.whattodo.data.api.request.TestRequest;
import tanchuev.whattodo.model.Employment;

public interface EmploymentApi {

	@POST("api/test/{id}")
	Observable<List<Employment>> testMethod(@Path("id") String id,
										   @Query("test") String test,
										   @Body TestRequest testRequest);

}