package tanchuev.whattodo.rx;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import tanchuev.whattodo.R;
import tanchuev.whattodo.rx.executor.PostExecutionThread;
import tanchuev.whattodo.rx.executor.ThreadExecutor;
import tanchuev.whattodo.rx.subscriber.BaseSubscriber;
import tanchuev.whattodo.ui.view.base.EmptyContentView;
import tanchuev.whattodo.ui.view.base.ErrorView;
import tanchuev.whattodo.ui.view.base.HasActionsView;
import tanchuev.whattodo.ui.view.base.LoadingView;
import tanchuev.whattodo.ui.view.base.NoInternetView;
import tanchuev.whattodo.utils.Utils;
import timber.log.Timber;

/**
 * @author tanchuev
 */
public class RxTaskExecutor {
	// класс для работы с многопоточностью с помощью Rx
	// один Presenter, один RxTaskExecutor

	private final List<Class<?>> NETWORK_EXCEPTIONS = Arrays.asList(
			UnknownHostException.class,
			SocketTimeoutException.class,
			ConnectException.class
	);

	private final ThreadExecutor mThreadExecutor;
	private final PostExecutionThread mPostExecutionThread;
	private CompositeSubscription mCompositeSubscription = new CompositeSubscription();

	//передается только в презентер
	@Inject
	protected RxTaskExecutor(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
		this.mThreadExecutor = threadExecutor;
		this.mPostExecutionThread = postExecutionThread;
	}

	public static void unsubscribe(Subscription subscription) {
		if (subscription != null && !subscription.isUnsubscribed()) {
			subscription.unsubscribe();
		}
	}

	public static boolean isNullOrUnsubscribed(Subscription subscription) {
		return subscription == null || subscription.isUnsubscribed();
	}

	public <T, TView extends HasActionsView> Subscription executeAsync(Observable<T> observable,
																	   @NonNull final Action1<? super T> doOnNext,
																	   @Nullable final Action1<Throwable> doOnError,
																	   @Nullable final Action0 doOnComplete,
																	   TView view) {
		observable = observable.compose(this::async);
		observable = addDoOnActions(observable, view, doOnError != null);

		Subscription subscription = observable
				.subscribe(new BaseSubscriber<>(
						doOnNext,
						doOnError,
						doOnComplete));
		mCompositeSubscription.add(subscription);

		return subscription;
	}

	@NonNull
	private <T, TView extends HasActionsView> Observable<T> addDoOnActions(Observable<T> observable,
																		   TView view,
																		   boolean overrideDoOnError) {
		if (view.hasActionView(LoadingView.class)) {
			observable = observable.compose(doOnLoad(view.getCurrentLoadingView()));
		}

		if (view.hasActionView(EmptyContentView.class)) {
			observable = observable.compose(doOnEmptyContent(view.getEmptyContentView()));
		}

		if (!overrideDoOnError) {
			ErrorView errorView = view.hasActionView(ErrorView.class) ? view.getErrorView() : null;
			NoInternetView noInternetView = view.hasActionView(NoInternetView.class) ? view.getNoInternetView() : null;

			if (errorView != null || noInternetView != null) {
				observable = observable.compose(doOnError(view.getErrorView(), view.getNoInternetView()));
			}
		}

		return observable;
	}

	@NonNull
	private <T> Observable<T> async(Observable<T> observable) {
		return observable
				.subscribeOn(Schedulers.from(mThreadExecutor))
				.observeOn(mPostExecutionThread.getScheduler());
	}

	@NonNull
	private <T> Observable.Transformer<T, T> doOnLoad(@NonNull LoadingView view) {
		return observable -> observable
				.doOnSubscribe(() -> {
					Timber.tag("LOADING").d("doOnSubscribe");
					view.showLoading();
				})
				/*.doOnTerminate(() -> {
                    Timber.tag("LOADING").d("doOnTerminate");
					view.hideLoading();
				})*/
				.doOnUnsubscribe(() -> {
					Timber.tag("LOADING").d("doOnUnsubscribe");
					view.hideLoading();
				});
	}

	@NonNull
	private <T> Observable.Transformer<T, T> doOnEmptyContent(@NonNull EmptyContentView emptyContentView) {
		return observable -> observable
				.doOnSubscribe(
						emptyContentView::hideNoContent
				)
				.flatMap(t -> {
					boolean isEmpty = false;
					if (t == null) {
						return Observable.empty();
					}
					if (t instanceof Collection) {
						isEmpty = ((Collection) t).isEmpty();
					} else if (t instanceof Map) {
						isEmpty = ((Map) t).isEmpty();
					}
					//todo add if(isEmptyResponse)
					return isEmpty ? Observable.empty() : Observable.just(t);
				})
				.switchIfEmpty(emptyObservable(emptyContentView));
	}

	private <T> Observable<T> emptyObservable(@NonNull EmptyContentView emptyContentView) {
		return (Observable<T>) (Observable
				.create(subscriber -> subscriber.onCompleted())
				.doOnCompleted(() ->
						emptyContentView.showNoContent()));
	}

	@NonNull
	private <T> Observable.Transformer<T, T> doOnError(@Nullable ErrorView errorView,
													   @Nullable NoInternetView noInternetView) {
		return observable -> observable
				.doOnSubscribe(() -> {
					if (noInternetView != null) {
						noInternetView.hideNoInternet();
					}
				})
				.doOnError(e -> {
					Timber.d(e, "from RxTaskExecutor.doOnError");
					Timber.e(e, e.getMessage());
					if (NETWORK_EXCEPTIONS.contains(e.getClass())) {
						if (noInternetView != null) {
							noInternetView.showNoInternet();
						} else if (errorView != null) {
							errorView.showError(R.string.errorNoInternet);
						}
					} else {
						if (errorView != null) {
							handleError(errorView, e);
						}
					}
				});
	}

	private void handleError(@NonNull ErrorView errorView,
							 Throwable e) {
		//todo add InternalAppException class for Internal Application errors and add .showError(internalAppException.getMessage())
        /*if (e instanceof HttpException) {
			final HttpException httpException = (HttpException) e;
			if (httpException.code() == 401) {
				final Context context = AndroidApplication.getAppContext();
				AuthUtils.logout(context, DataManager.get());
				AuthUtils.openLoginActivity(context);
			} else {
				try {
					final String errorBody = httpException.response().errorBody().string();
					final ErrorBean errorBean = GsonUtil.requestGson().fromJson(errorBody, ErrorBean.class);
					view.showError(errorBean.getFirstErrorMessage());
				} catch (IOException | IllegalStateException e1) {
					view.showError(httpException.message());
				}
			}
		} else */
		if (Utils.isNullOrEmpty(e.getMessage())) {
			errorView.showError(R.string.errorUnexpected);
		} else {
			errorView.showError(e.getMessage());
		}
	}

	public void clearSubscriptions() {
		mCompositeSubscription.clear();
	}

	public void unsubscribeAll() {
		mCompositeSubscription.unsubscribe();
	}
}