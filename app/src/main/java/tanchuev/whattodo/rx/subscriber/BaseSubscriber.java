package tanchuev.whattodo.rx.subscriber;

import rx.Subscriber;
import rx.functions.Action0;
import rx.functions.Action1;


/**
 * @author tanchuev
 */
public class BaseSubscriber<T> extends Subscriber<T> {

	private Action1<? super T> onNext;
	private Action1<Throwable> onError;
	private Action0 onCompleted;

	public BaseSubscriber(Action1<? super T> onNext,
						  Action1<Throwable> onError,
						  Action0 onCompleted) {
		this.onNext = onNext;
		this.onError = onError;
		this.onCompleted = onCompleted;
	}

	@Override
	public void onCompleted() {
		if (onCompleted != null) {
			onCompleted.call();
		}
	}

	@Override
	public void onError(Throwable e) {
		if (onError != null) {
			onError.call(e);
		}
	}

	@Override
	public void onNext(T t) {
		if (onNext != null) {
			onNext.call(t);
		}
	}
}
