package tanchuev.whattodo.event;

import java.util.List;

import tanchuev.whattodo.model.Category;

/**
 * @author tanchuev
 */

public class SelectCategoriesEvent {
	private List<Category> mCategories;

	public SelectCategoriesEvent(List<Category> categories) {
		mCategories = categories;
	}

	public List<Category> getCategories() {
		return mCategories;
	}
}
