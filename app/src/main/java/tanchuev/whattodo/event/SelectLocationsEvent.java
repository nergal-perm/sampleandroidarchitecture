package tanchuev.whattodo.event;

import java.util.List;

import tanchuev.whattodo.model.Location;

/**
 * @author tanchuev
 */

public class SelectLocationsEvent {
	private List<Location> mLocations;

	public SelectLocationsEvent(List<Location> locations) {
		mLocations = locations;
	}

	public List<Location> getLocations() {
		return mLocations;
	}
}
