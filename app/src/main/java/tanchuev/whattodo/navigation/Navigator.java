package tanchuev.whattodo.navigation;

import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;

import tanchuev.whattodo.ui.dialog.base.ActionsDialogFragment;

public interface Navigator {

	void showDialog(ActionsDialogFragment dialogFragment);

	@IdRes
	int getFragmentContainer();

	void startActivity(Class toActivityClass, boolean closePreviousActivity);

	void addFragment(Fragment fragment);

	void addFragment(@IdRes int containerViewId, Fragment fragment);

	void replaceFragment(Fragment fragment);

	void replaceFragment(@IdRes int containerViewId, Fragment fragment, boolean withAnimation);

	void back();
}
