package tanchuev.whattodo.navigation;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import javax.inject.Inject;

import tanchuev.whattodo.R;
import tanchuev.whattodo.ui.activity.base.InjectorActivity;
import tanchuev.whattodo.ui.dialog.base.ActionsDialogFragment;
import tanchuev.whattodo.utils.ViewUtils;

public class NavigatorImpl implements Navigator {

	private InjectorActivity mActivity;
	private FragmentManager mFragmentManager;

	@Inject
	public NavigatorImpl(InjectorActivity activity) {
		mActivity = activity;
		mFragmentManager = mActivity.getSupportFragmentManager();
	}

	@Override
	public void showDialog(ActionsDialogFragment dialogFragment) {
		dialogFragment.show(mFragmentManager, dialogFragment.getClass().getSimpleName());
	}

	@Override
	public int getFragmentContainer() {
		return R.id.fragmentContainer;
	}

	@Override
	public void startActivity(Class toActivityClass, boolean closePreviousActivity) {
		Intent intent = new Intent(mActivity, toActivityClass);
		mActivity.startActivity(intent);
		if (closePreviousActivity)
			mActivity.finish();
	}

	@Override
	public void addFragment(Fragment fragment) {
		addFragment(getFragmentContainer(), fragment);
	}

	@Override
	public void addFragment(int containerViewId, Fragment fragment) {
		FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
		fragmentTransaction.add(containerViewId, fragment);
		fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
		fragmentTransaction.commit();
	}

	@Override
	public void replaceFragment(Fragment fragment) {
		replaceFragment(getFragmentContainer(), fragment, true);
	}

	@Override
	public void replaceFragment(int containerViewId, Fragment fragment, boolean withAnimation) {
		ViewUtils.hideKeyboard(mActivity);

		FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
		fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
		if (withAnimation) {
			fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		}
		fragmentTransaction.replace(containerViewId, fragment);
		fragmentTransaction.commit();
	}

	@Override
	public void back() {
		mFragmentManager.popBackStack();
	}
}
